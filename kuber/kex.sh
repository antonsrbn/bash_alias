#!/bin/bash
podName=$(kubectl get po | grep $1 | awk '{print $1}' | awk 'END{print}');
echo $podName;
kubectl exec -ti $podName -- bash