#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

if [[ $1 != '' ]]; then
	docker ps --format '{{.Names}}\t\t\t{{.Command}}\t\t{{.Status}}\t\t{{.Ports}}' -a | grep $1;
else
 	docker-compose ps
fi

