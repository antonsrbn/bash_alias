#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

if [[ $2 == '-u' ]]; then
	user=1000;
	if [[ $3 != '' ]]; then
		user=$3;
	fi	
	docker-compose exec -u$user $1 bash	
elif [[ $2 != '' ]]; then
	docker-compose run --entrypoint $2 $1 && docker container prune -f
else
	docker-compose exec $1 bash
fi
