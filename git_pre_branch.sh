#!/bin/bash

param3=$3;

function getRepName {
	repName=$(git remote | grep origin);
	if [[ $currentRep == '' ]];
	then
	    repName='origin'
	else
	    repName=$(git remote | awk '{print $1; exit}');
	fi
}

function getTaskRefs {

	taskRefs=$(git log  HEAD^..HEAD | grep -oP "[Rr]{1}ef(s){1}(:)?.+" | grep -oP '[A-Z0-9]{1,5}-\d{1,6}' | uniq | xargs | sed 's/ /, /g');
	if [[ $param3 != '' ]];
	then
	    taskRefs=$param3;
	fi
	if [[ $taskRefs == '' ]];
	then
	    echo -e "\e[35mWARN: Not tasks in refs";
	fi
	
	branchName=$(git rev-parse --abbrev-ref HEAD);


    if [[ $param3 != '' ]];
    then
        refs=$param3;
    elif [[ $(echo $branchName | grep -E '^[A-Z0-9]{1,5}-[0-9]{1,6}') != ''  ]]; then
        refs=$(echo $branchName | sed -E 's/([A-Z0-9]{1,5}-[0-9]{1,6}).*/\1/g');
    elif [[ $taskRefs == *"$branchName"* ]]; then
  	    refs="$taskRefs";
    else
    	 if [[ $taskRefs == '' ]]; then
    		refs="$branchName";
    	 else
    		refs="$branchName, $taskRefs";
    	 fi
    fi
}

function confirm {
    echo -n "Подтвердите действие (y|Y|1|yes|Yes|YES|Да|да|д|Д|true) "

    read item
    case "$item" in
        y|Y|1|yes|Yes|YES|Да|да|д|Д|true) echo "Выполняем..."
            ;;
        *) echo "Отменено."
            exit 0
            ;;
    esac
}

function checkAllowPush {
  filePath="/home/$USER/git_disallow_push_branches"
  if [ ! -f $filePath ]; then
    echo -e "\e[33mWARN: File $filePath for Disallow push branches found!\e[0m";
  else
    repoUrl=$(git remote get-url --push --all ${1})
    blackItem="$repoUrl::${2}";
    while read lineItem; do
        if [[ $lineItem == $blackItem ]];
        then
            echo -e "\e[31mERROR: Denied push to '$blackItem'! See in black list file '$filePath'.\e[0m";
            exit 0;
        fi
    done <$filePath
  fi
}


function printLastCommit {
    lastCommits=$(git log  HEAD^..HEAD | sed -E 's/^commit\s.+//g' | sed -E 's/^Author:\s.+//g' \
        | sed -E 's/^Date:\s.+//g' | sed -E 's/\s\s\s\sMerge\sbranch\s'.+'\sinto\s.+//g' | sed -E 's/^Merge:\s.+//g' \
        | sed -E 's/\s\s\s\sSee\smerge\srequest\s.+//g' | sed -E 's/\s\s\s\sCloses\s.+//g' \
        | sed -E 's/\s+/ /g' | sed -E 's/^\s$//g'  | sed -E 's/^\s(.+)$/\1/g' | grep -E  '.+' \
        | grep -E "[Rr]{1}ef(s){1}(:)?\s*(${1}|${2})" | sed -E 's/[Rr]{1}ef(s){1}(:)?.+//g' \
        | grep -E  -m1 '.+' \
        );
    
    if [[ $lastCommits == '' ]];
        then
            lastCommits=$(git log  HEAD^..HEAD | sed -E 's/^commit\s.+//g' | sed -E 's/^Author:\s.+//g' \
        | sed -E 's/^Date:\s.+//g' | sed -E 's/\s\s\s\sMerge\sbranch\s'.+'\sinto\s.+//g' | sed -E 's/^Merge:\s.+//g' \
        | sed -E 's/\s\s\s\sSee\smerge\srequest\s.+//g' | sed -E 's/\s\s\s\sCloses\s.+//g' \
        | sed -E 's/\s+/ /g' | sed -E 's/^\s$//g'  | sed -E 's/^\s(.+)$/\1/g' \
        | grep -E  -m1 '.+' \
        )
        fi
    
    lastCommits=$(echo $lastCommits | sed -E 's/\s+$//g');

    commitPrefix=$(echo $lastCommits | sed -E 's/(.+)(\(.+\))(:)?.+/\1\2\3/g');
    if [[ $lastCommits == $commitPrefix ]]; then
       commitPrefix='';
    fi
    changeCommand=$(echo $commitPrefix | sed -E 's/^([a-zA-Z0-9]+)\.([a-z]{1,10}).+/\1/g');
    if [[ $changeCommand == $commitPrefix ]]; then
       changeCommand='';
    fi
    changeType=$(echo $commitPrefix | sed -E "s/^$changeCommand\.([a-z]{1,10})\(.+/\1/g");
    if [[ $changeType == $commitPrefix ]]; then
       changeType=$(echo $changeType | sed -E "s/^([a-z]{1,10})\(.+/\1/g");
    fi
    if [[ $changeType == $commitPrefix ]]; then
       changeType='';
    fi
    changeContext=$(echo $commitPrefix | sed -E 's/.+\(([a-zA-Z0-9\.\_\-]+)\)(:)?\s*$/\1/g');
    if [[ $changeContext == $commitPrefix ]]; then
       changeContext='';
    fi
   
   if [[ $3 != '1' ]]; then
        echo -e "\e[34mCommit prefix: \e[0m$commitPrefix\e[0m \e[34mCommit command: \e[0m$changeCommand\e[0m \e[34mCommit type: \e[0m$changeType\e[0m \e[34mCommit context: \e[0m$changeContext\e[0m";
        echo -e "\e[33mLast Commit:\e[0m";
        echo $lastCommits;
   fi
}

function validateCommitPrefix {
    __commit=${1};
    __commitPrefix=$(echo $__commit | sed -E 's/(.+)(\(.+\))(:)?.+/\1\2\3/g');
    if [[ $__commit == $__commitPrefix ]]; then
       __commitPrefix='';
    fi
    if [[ $__commitPrefix == '' ]]; then
        echo -e "\e[31mCOMMIT PREFIX ERROR: Prefix is invalid.\e[0m";
        exit 0;
    fi
}



function prepareCommit {
    prepareCommit_lastCommitPrefix=${1};
    prepareCommit_lastCommitChangeCommand=${2};
    prepareCommit_lastCommitChangeContext=${3};
    prepareCommit_currentCommit=${4};

    prepareCommit_newCommit=$prepareCommit_currentCommit;

    


    if [[ $prepareCommit_lastCommitPrefix == '' ]]; then
      return;
    fi




    prepareCommit_commitPrefix=$(echo $prepareCommit_currentCommit | sed -E 's/(.+)(\(.+\))(:)?.+/\1\2\3/g');
    if [[ $prepareCommit_currentCommit != $prepareCommit_commitPrefix ]]; then
        return;
    fi


    prepareCommit_commityType=$(echo $prepareCommit_currentCommit | sed -E 's/.*(fix|feat).*/\1/g');
    if [[ $prepareCommit_currentCommit != $prepareCommit_commityType ]]; then
        prepareCommit_clearCommit=$(echo $prepareCommit_currentCommit | sed -E 's/(fix|feat)\s(.*)/\2/g' | sed -E 's/(.*)(fix|feat)\s(.*)/\1\3/g' | sed -E 's/\s+/ /g');

        if [[ $prepareCommit_lastCommitChangeCommand != '' ]]; then
            prepareCommit_newCommit="$prepareCommit_lastCommitChangeCommand.$prepareCommit_commityType($prepareCommit_lastCommitChangeContext): $prepareCommit_clearCommit";
        else    
            prepareCommit_newCommit="$prepareCommit_commityType($prepareCommit_lastCommitChangeContext): $prepareCommit_clearCommit";
        fi
        return;
    fi




    if [[ $prepareCommit_lastCommitPrefix != '' ]]; then
      prepareCommit_newCommit="$prepareCommit_lastCommitPrefix $prepareCommit_newCommit";
      return;
    fi

}


if [[ ( $1 == 'branch' && $2 != '' && $2 != '-'* ) || ( $1 == 'checkout' && $2 == '-b' ) ]];
then
    branchName=$(git rev-parse --abbrev-ref HEAD);

    getTaskRefs $branchName;
    getTaskRefs;
    echo -e "\e[92mRefs: $refs";
    printLastCommit $branchName "$refs";

    if [[ $branchName == 'master' ]];
    then
	git $@
    else
       git $@
    fi
elif [[ $1 == 'branch-imp' ]];
then
   branchName=$(git rev-parse --abbrev-ref HEAD);
   git checkout -b "$branchName->iss$2"
elif [[ $1 == 'add' ]];
then
    git $@;
    git status
elif [[ $1 == 'bbcp' ]];
then
    git $@;
    git status
elif [[ $1 == 'bbcp' ]];
then
    branchName=$(git rev-parse --abbrev-ref HEAD);
    git branch nomerge-$branchName-backup;
    echo -e "\e[92mВетка забэкапена в 'nomerge-$branchName-backup'"
elif [[ $1 == 'bbcin' ]];
then
    branchName=$(git rev-parse --abbrev-ref HEAD);
    git checkout nomerge-$branchName-backup;
    echo -e "\e[92mНа бекапе ветки '$branchName'"
elif [[ $1 == 'bbcout' ]];
then
    bmOrigin=$(git rev-parse --abbrev-ref HEAD  | sed "s/nomerge-//" | sed "s/-backup//");
    git checkout $bmOrigin;
    echo -e "\e[92mИз бекапа в оригинал ветки '$bmOrigin'"
elif [[ $1 == 'checkout' ]];
then
    git $@;
    git status;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    echo -e "\e[92m$branchName";
    getTaskRefs;
    echo -e "\e[92mRefs: $refs";
    printLastCommit $branchName "$refs";
elif [[ $1 == 'reset' ]];
then
    git $@;
    git status
elif [[ $1 == 'cm' ]];
then

    getRepName;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    checkAllowPush $repName $branchName;

    getTaskRefs;

    git status;

    commitName=$2
    printLastCommit $branchName "$refs" "1";
    if [[ $commitName == '' ]]; then
       commitName=$lastCommits
    fi

    commitName=$(echo $commitName | sed -E 's/\s+/ /g');


    prepareCommit "$commitPrefix" "$changeCommand" "$changeContext" "$commitName";
    commitName=$prepareCommit_newCommit;


    echo "Текст коммита: $commitName Refs: $refs";
    echo "Будет коммит в ветку: $branchName";

    validateCommitPrefix "$commitName";

    confirm;


    git commit -m"$commitName Refs: $refs";
elif [[ $1 == 'cmph' || $1 == 'cpmh' ]];
then
    getRepName;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    checkAllowPush $repName $branchName;

    getTaskRefs;

    git status;

    commitName=$2
    printLastCommit $branchName "$refs" "1";
    if [[ $commitName == '' ]]; then
       commitName=$lastCommits
    fi

    commitName=$(echo $commitName | sed -E 's/\s+/ /g');

    prepareCommit "$commitPrefix" "$changeCommand" "$changeContext" "$commitName";
    commitName=$prepareCommit_newCommit;

    echo "Текст коммита: $commitName Refs: $refs";
    echo "Будет коммит и пуш в ветку: $branchName";

    validateCommitPrefix "$commitName";

    confirm;


    git commit -m"$commitName Refs: $refs";

    #git pull $repName $branchName
    git push $repName $branchName
elif [[ $1 == 'cmphm' || $1 == 'cpmhm' ]];
then
    getRepName;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    checkAllowPush $repName $branchName;

    getTaskRefs;

    git status;

    echo "Текст коммита: $2 Refs: $refs";
    echo "Будет коммит и пуш в ветку: $branchName";
    confirm;

    git commit -m"$2 Refs: $refs";
    #git pull $repName $branchName;
    git push $repName $branchName;
    git checkout master;
    getRepName;
    git pull $repName master;
    git checkout $branchName;
    git merge master;
    git push $repName $branchName;
    git checkout master;
    git merge $branchName;
    git push origin master;
    git checkout $branchName;
elif [[ $1 == 'cmpht' || $1 == 'cpmht' ]];
then
    getRepName;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    checkAllowPush $repName $branchName;

    getTaskRefs;

    echo "Текст коммита: $2 Refs: $refs";
    echo "Будет коммит и пуш в ветку: $branchName";

    confirm;

    git commit -m"$2 Refs: $refs";
 
    #git pull $repName $branchName;
    git push $repName $branchName;
    git checkout test;
    branchNameCheck=$(git rev-parse --abbrev-ref HEAD);
    if [[ $branchNameCheck == 'test' ]];
    then
    	getRepName;
    	git pull $repName test;
        echo -e "\e[92mПереключили и спулили в тест: $branchNameCheck\e[92m";
	git merge $branchName;
 	git push $repName $branchNameCheck;
	git checkout $branchName;
    else
        echo -e "\e[31mОшибка. Текущая ветка $branchName"
    fi
  
elif [[ $1 == 'bm' ]];
then
    git checkout master;
    if [[ $3 != '-nopull' ]];
    then
        getRepName;
        branchName=$(git rev-parse --abbrev-ref HEAD);
        git pull $repName $branchName;
    fi
    git checkout -b iss$2;
elif [[ $1 == 'bmc' ]];
then
    git checkout master;
    if [[ $3 != '-nopull' ]];
    then
        getRepName;
        branchName=$(git rev-parse --abbrev-ref HEAD);
        git pull $repName $branchName;
    fi
    git checkout -b $2
elif [[ $1 == 'ph' ]];
then
    getRepName;
    branchName=$(git rev-parse --abbrev-ref HEAD);

    checkAllowPush $repName $branchName;

    git status;

    echo "Пуш в: $repName/$branchName $2";
    confirm;

    
    #git pull $repName $branchName
    git push $repName $branchName $2
elif [[ $1 == 'pl' ]];
then
    getRepName;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    git pull $repName $branchName
elif [[ $1 == 'main' ]];
then
    git checkout master;
    getRepName;
    git pull $repName master;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    echo -e "\e[92m$branchName"
elif [[ $1 == 'test' ]];
then
    git checkout test;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    if [[ $branchName == 'test' ]];
    then
    	getRepName;
    	git pull $repName test;
        echo -e "\e[92m$branchName"
    else
        echo -e "\e[31mОшибка. Текущая ветка $branchName"
    fi
elif [[ $1 == 'merm' ]];
then
   branchName=$(git rev-parse --abbrev-ref HEAD);
   git checkout master;
   getRepName;
   git pull $repName master;
   git checkout $branchName;
   git merge master
elif [[ $1 == 'branch' && $2 == '' ]];
then
    branchName=$(git rev-parse --abbrev-ref HEAD);
    echo -e "\e[92m$branchName";
    getTaskRefs;
    echo -e "\e[92mRefs: $refs";
    printLastCommit $branchName "$refs";
elif [[ $1 == 'st' ]];
then
    git status;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    echo -e "\e[92m$branchName";
    getTaskRefs $branchName;
    echo -e "\e[92mRefs: $refs";
    printLastCommit $branchName "$refs";
elif [[ $1 == 'sb' ]];
then
    # Получаем список веток, отсортированных по дате последнего коммита
    branches=$(git for-each-ref --sort=-committerdate --format='%(refname:short)' refs/heads/ | head -n 15)

    # Проверяем, есть ли ветки
    if [ -z "$branches" ]; then
        echo "\e[34mНет доступных веток.\e[0m"
        exit 1
    fi

    # Выводим список веток с нумерацией
    echo -e "\e[34mНедавно использованные ветки:\e[0m"
    select branch in $branches; do
        if [ -n "$branch" ]; then
            echo -e "\e[32mВы выбрали ветку:\e[33m $branch\e[0m"
            git checkout "$branch";
            break;
        else
            echo -e "\e[31mДействие отменено.\e[0m";
            break;     
        fi
    done
elif [[ $1 == 'lb' ]];
then
    # Получаем список веток, отсортированных по дате последнего коммита
    echo -e "\e[34mНедавно использованные ветки:\e[0m"
    git for-each-ref --sort=-committerdate --format='%(refname:short)' refs/heads/ | head -n 15;
elif [[ $1 == 'fh' && $2 != '' ]];
then
    getRepName;
    git fetch $repName $2;
    git checkout $2;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    if [[ $2 == $branchName ]];
    then
		echo -e "\e[92mFetch and checkout on branch '$2'";		
    else
        issBranchName="iss$2";
		echo -e "\e[35mTry: Fetch and checkout on branch '$issBranchName'";
		git fetch $repName $issBranchName;
        git checkout $issBranchName;
        branchName=$(git rev-parse --abbrev-ref HEAD);
	if [[ $issBranchName == $branchName ]];
	then
		echo -e "\e[92mFetch and checkout on branch '$issBranchName'";			
	else
		echo -e "\e[31mError: remote branch '$2' and '$issBranchName' not found!";	
	fi
    fi
elif [[ $1 == 'hard' ]];
then
    getRepName;
    branchName=$(git rev-parse --abbrev-ref HEAD);
    git fetch $repName $branchName && git reset --hard $repName/$branchName
elif [[ $1 == 'helpext' ]];
then
    echo -e "\e[32mКоманды:\e[92m
cm \"\e[35m[комментарий]\e[92m\"       \e[91m=\e[92m git commit -m\"\e[35m[текущая ветка]\e[92m|\e[35m[комментарий]\e[92m\"
cmph,cpmh \"\e[35m[комментарий]\e[92m\"\e[91m=\e[92m git commit -m\"\e[35m[текущая ветка]\e[92m|\e[35m[комментарий]\e[92m\"; git ph
bm \e[35m[номер задачи]\e[92m        \e[91m=\e[92m git checkout master; git pl (будет выполнено если 3 параметр не -nopull); git checkout -b iss\e[35m[номер задачи]\e[92m
bmc \e[35m[название ветки]\e[92m     \e[91m=\e[92m git checkout master; git pl (будет выполнено если 3 параметр не -nopull); git checkout -b \e[35m[название ветки]\e[92m
branch-imp \e[35m[номер ветки]\e[92m \e[91m=\e[92m git checkout -b \"\e[35m[текущая ветка]\e[92m->iss\e[35m[номер задачи]\e[92m\"
bbcp	    	         \e[91m=\e[92m git branch nomerge-\e[35m[текущее название ветки]\e[92m-backup - создает бэкап ветки
bbcin		         \e[91m=\e[92m переключается на бэкап ветки
bbcout		         \e[91m=\e[92m переключается с бэкапа на оригинал ветки
ph                       \e[91m=\e[92m git push \e[35m[текущий репозиторй]\e[92m \e[35m[текущая ветка]\e[92m
pl                       \e[91m=\e[92m git pull \e[35m[текущий репозиторй]\e[92m \e[35m[текущая ветка]\e[92m
main                     \e[91m=\e[92m git checkout master; git pull \e[35m[текущий репозиторй]\e[92m master
test                     \e[91m=\e[92m git checkout test; git pull \e[35m[текущий репозиторй]\e[92m test
hard                     \e[91m=\e[92m git fetch \e[35m[текущий репозиторй]\e[92m \e[35m[текущая ветка]\e[92m && git reset --hard \e[35m[текущий репозиторй]\e[92m/\e[35m[текущая ветка]\e[92m
merm                     \e[91m=\e[92m git main; git checkout \e[35m[текущая ветка(не master)]\e[92m; git merge master
branch                   \e[91m=\e[92m покажет текущую ветку и ссылку на задачу
branch \e[35m[название ветки]\e[92m и 
git checkout -b \e[35m[название
 ветки]\e[92m                  \e[91m=\e[92m разрешит создать ветку только от master
add, checkout, reset     \e[91m=\e[92m дополнительно ещё выполнит команду git status [ch - checkout, rs - reset]
st                       \e[91m=\e[92m git status
cmphm                    \e[91m=\e[92m git cmph; git merge branch in master; git ph \e[31m(Внимание! Очень опасная команда!)\e[92m
cmpht                    \e[91m=\e[92m git cmph; git merge branch in test; git ph \e[31m(Внимание! Очень опасная команда, но менее опаснее, чем cmphm!)\e[92m
fh \e[35m[название ветки]\e[92m\e[91m      =\e[92m git fetch \e[35m[текущий репозиторй]\e[92m \e[35m[текущая ветка]\e[92m; git checkout
"
else
    git $@
fi
