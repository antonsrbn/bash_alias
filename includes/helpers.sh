#!/bin/bash

source $(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/log.sh

executingDir=$(pwd);
function checkLinkDirs {
  filePath="/home/$USER/bash_alias_link_dir"
  if [ ! -f $filePath ]; then
    log-warn "File $filePath for link dirs found!";
  else
   
    executingDir=$(pwd);
    while read lineItem; do
        if [[ $lineItem =~ ^"${executingDir}:" ]];
        then
            link=$(echo "$lineItem" | sed -E "s/^(.+):(.+)$/\2/g");
            log-success "Linked from: $link";
            executingDir=$link;
        fi
    done <$filePath
  fi
}