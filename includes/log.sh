function log-warn () {
    result="";
    result="\e[33m${1}\e[0m";
    echo -e ${result};
}
function log-error () {
    result="";
    result="\e[31m${1}\\e[0m";
    echo -e ${result};
}
function log-success () {
    result="";
    result="\e[32m${1}\e[0m";
    echo -e ${result};
}
function log-notice () {
    result="";
    result="\e[34m${1}\e[0m";
    echo -e ${result};
}
function log-section () {
    result="";
    result="\e[1;39;40m${1}\e[0m";
    echo -e ${result};
}
function log-line () {
    result="";
    result="\e[0m\e[0m";
    echo -e ${result};
}