#!/bin/bash
projectName=$(cat .env | grep -E "COMPOSE_PROJECT_NAME=.+?" | sed 's/^COMPOSE_PROJECT_NAME=//');

containerPostfix=$1;

containerId=$(docker ps -a | grep "${projectName}_${containerPostfix}" | awk '{print $1}');

ip=$(docker inspect $containerId | grep IPAddress | awk 'END{print}' | sed 's/"IPAddress": "//' | sed 's/",//' | awk '{print $1}');
echo -e "\e[39mContainer \e[36m${projectName}\e[39m_\e[35m${containerPostfix}\e[39m IP: \e[33m$ip";	

