#!/bin/bash
projectName=$(cat .env | grep -E "COMPOSE_PROJECT_NAME=.+?" | sed 's/^COMPOSE_PROJECT_NAME=//');
containerId=$(docker ps -a | grep "${projectName}_php" | awk '{print $1}');

command='migrate';
if [[ $1 == '-r' ]];
then
	command="$command:rollback --step";
	if [[ $2 == '' ]];
	then 
		command="$command=1";
	else
		command="$command=$2";
	fi
	echo -e "\e[94mОткат миграций Laravel..";
elif [[ $1 == '-c' && $2 != '' ]];
then
	command="make:migration $2";
	echo -e "\e[94mСоздание миграции '$2' Laravel..";
else
	echo -e "\e[94mЗапуск миграций Laravel..";
fi

docker exec -it -u1000 $containerId php /var/www/html/artisan $command;
echo -e "\e[92mМиграции отработаны";
