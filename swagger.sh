#!/bin/bash
projectName=$(cat .env | grep -E "COMPOSE_PROJECT_NAME=.+?" | sed 's/^COMPOSE_PROJECT_NAME=//');
containerId=$(docker ps -a | grep "${projectName}_php" | awk '{print $1}');
echo -e "\e[94mЗапуск l5-swagger:generate Laravel..";
docker exec -it -u1000 $containerId php /var/www/html/artisan l5-swagger:generate;
echo -e "\e[92ml5-swagger:generate отработан";
