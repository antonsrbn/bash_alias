#!/bin/bash
path=$(cat .env | grep -E "catalogSRV=.+?" | sed 's/^catalogSRV=//');
user=$(cat .env | grep -E "userSRV=.+?" | sed 's/^userSRV=//');
port=$(cat .env | grep -E "portSRV=.+?" | sed 's/^portSRV=//');
host=$(cat .env | grep -E "^SRV=.+?" | sed 's/^SRV=//');
alpath=$(cat .env | grep -E "SSH$1SRV=.+?" | sed "s/SSH$1SRV=//");
if [[ $1 != '' ]]; 
then
	if [[ $alpath != '' ]];
	then 
		path=$alpath
	else
		echo -e "\e[31mАлиас для $1 не найден";
		exit 1
	fi	
fi
ssh -p$port -t $user@$host "cd $path;ls;brnchnm=\$(git rev-parse --abbrev-ref HEAD);echo -e \"
\e[93mТекущая ветка:\e[92m\e[1m\$brnchnm
\e[0m\";git status; bash"



