#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

if [[ $1 != '' ]]; then
  containerNames=$(docker ps --format '{{.Names}}' -a | grep $1);
else
  containerNames=$(docker-compose ps -a | awk '{print $1}');
fi

iterable=0;
while read -r line; do
   if [[ ( $1 == ''  &&  $iterable > 1 ) || ( $1 != '' && $line != '' ) ]];
   then
    ip=$(docker inspect $line | grep IPAddress | awk 'END{print}' | sed 's/"IPAddress": "//' | sed 's/",//' | awk '{print $1}')
    echo -e "\e[1;33;40m$line\e[37m\t\t: \e[1;33;35m$ip\e[0m\e[37m"
   fi;
  iterable=$(($iterable + 1));
done <<< "$containerNames"
