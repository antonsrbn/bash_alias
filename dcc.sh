#!/bin/bash
find bootstrap/cache/ -type f -name "*.php" -exec rm {} \;
echo -e "\e[92mBootstrap кэш удален";
projectName=$(cat .env | grep -E "COMPOSE_PROJECT_NAME=.+?" | sed 's/^COMPOSE_PROJECT_NAME=//');
containerId=$(docker ps -a | grep "${projectName}_php" | awk '{print $1}');
echo -e "\e[94mОбновление кеша конфигов Laravel..";
docker exec -it -u1000 $containerId php /var/www/html/artisan config:cache;
echo -e "\e[92mОбновление кеша конфигов отработано";
