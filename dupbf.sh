#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

docker-compose build && docker-compose up -d --force-recreate && docker-compose ps