echo -e "\e[92mbitrix->tools:";
if [[ $1 == 'ruen' ]];
then
	echo -e "\e[94mruen start..";
	BX_DOC_ROOT=$2'/';
	echo -e "\e[95mset DOCUMENT_ROOT project done..\e[32m";
	path=$(pwd)'/'$3;
	php ~/bash_alias/php_tools/bitrix_include_field_ru_en.php $BX_DOC_ROOT $path;
	echo -e "\e[96mruen done.";
elif [[ $1 == 'custom' ]];
then
	echo -e "\e[94mcustom generate start..";
        COMPONENT_NAME=$2;
	if [[ $2 == '' ]]
	then
		echo -e "\e[91mSet, please, component name in first arquments!";
		exit;
	fi

	echo -e "\e[95mset COMPONENT_NAME project done..\e[32m";
	CONCTRETE_LOCAL_PATH=$3;
	if [[ $CONCTRETE_LOCAL_PATH == '' ]]
	then
		CONCTRETE_LOCAL_PATH=$(pwd);
	fi

	echo -e "\e[95mset CONCTRETE_LOCAL_PATH project done..\e[32m";
	php ~/bash_alias/php_tools/bx.custom_component.generate/main.php $COMPONENT_NAME $CONCTRETE_LOCAL_PATH'/components/custom/';
	echo -e "\e[96mcustom done.";
fi
