#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

containerId=$(docker ps -a | grep -E "\s$1.+?_db$" | awk '{print $1}');
cdate=local_dump_$1_$(date +'date%Y.%m.%d_time%H.%M.%S').sql;
#docker exec -it $containerId mysqldump -udb -pdb -hdb db > $cdate;
docker exec -it $containerId mysqldump -uroot -proot -hdb db > $cdate;
echo "$cdate"

