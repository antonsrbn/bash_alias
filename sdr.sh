#!/bin/bash
projectName=$(cat .env | grep -E "COMPOSE_PROJECT_NAME=.+?" | sed 's/^COMPOSE_PROJECT_NAME=//');
containerId=$(docker ps -a | grep "${projectName}_php" | awk '{print $1}');
path=$1;
docker exec -it $containerId sed -i "/DocumentRoot  \/var\/www\/html/c\DocumentRoot  \/var\/www\/html\/$path" /etc/apache2/conf-enabled/40-user.conf;
docker exec -it $containerId apachectl graceful;
echo -e "\e[92mУстановлен \e[35mDocumentRoot /var/www/html/$path";
