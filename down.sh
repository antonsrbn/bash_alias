#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

if [[ $1 != '' ]]; then
  containerNames=$(docker ps --format '{{.Names}}' -a | grep $1);

 
while read -r line; do
   docker kill $line;
   docker rm $line;
   echo -e "\e[1;33;40m$line\e[37m\t\t: \e[1;33;35m stop\e[0m\e[37m"
done <<< "$containerNames"

docker network prune -f

else
  docker-compose down && docker-compose ps
fi
echo -e "\e[31mПроект остановлен";
