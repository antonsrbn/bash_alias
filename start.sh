#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
docker-compose start $1 && bash "$SCRIPT_DIR/dps.sh" && bash "$SCRIPT_DIR/cip.sh"
echo -e "\e[92mКонтейнер $1 поднят";
