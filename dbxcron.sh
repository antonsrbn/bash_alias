#!/bin/bash
projectName=$(cat .env | grep -E "COMPOSE_PROJECT_NAME=.+?" | sed 's/^COMPOSE_PROJECT_NAME=//');
containerId=$(docker ps -a | grep "${projectName}_php" | awk '{print $1}');
echo -e "\e[94mЗапуск cron_events.php битрикса..";
docker exec -it -u1000 $containerId php /var/www/html/bitrix/modules/main/tools/cron_events.php;
echo -e "\e[92mcron_events.php битрикса отработан";
