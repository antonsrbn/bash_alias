#!/bin/bash
projectName=$(cat .env | grep -E "COMPOSE_PROJECT_NAME=.+?" | sed 's/^COMPOSE_PROJECT_NAME=//');

phpContainerId=$(docker ps -a | grep "${projectName}_php" | awk '{print $1}');
dbContainerId=$(docker ps -a | grep "${projectName}_db" | awk '{print $1}');

phpIp=$(docker inspect $phpContainerId | grep IPAddress | awk 'END{print}' | sed 's/"IPAddress": "//' | sed 's/",//' | awk '{print $1}');
echo -e "\e[29mContainer php IP: \e[35m$phpIp";

dbIp=$(docker inspect $dbContainerId | grep IPAddress | awk 'END{print}' | sed 's/"IPAddress": "//' | sed 's/",//' | awk '{print $1}');
echo -e "\e[39mContainer db  IP: \e[33m$dbIp";	

