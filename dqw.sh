#!/bin/bash
projectName=$(cat .env | grep -E "COMPOSE_PROJECT_NAME=.+?" | sed 's/^COMPOSE_PROJECT_NAME=//');
containerId=$(docker ps -a | grep "${projectName}_php" | awk '{print $1}');

command="queue:work";

if [[ $1 == '' ]];
then
	echo -e "\e[94mСтарт воркера очередей Laravel..";	
else
	if [[ $2 == '' ]];
	then
		echo -e "\e[94mСтарт воркера очереди \e[93m[$1]\e[94m Laravel..";
		command="$command --queue='$1'";	
	else
		echo -e "\e[94mСтарт воркера очереди \e[93m[$1]\e[94m, таймаут \e[92m[$2 секунд]\e[94m Laravel..";
		command="$command --queue='$1' --timeout=$2";
	fi

fi

echo -e "\e[95martisan command: [\e[96m$command\e[95m]\e[97m";

docker exec -it -u1000 $containerId php /var/www/html/artisan $command;
