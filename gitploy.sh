#!/bin/bash
cd /home/anton/projects;
git clone git@gitlab.com:webpractik/$1.git /home/anton/projects/$1;
cd /home/anton/projects/$1 && wpdeploy env && wpdeploy deploy && composer install && cd /local/static/ && npm run gulp;
echo -e "\e[92mПроект развернут (внимание: скрипт не доконца усовершенствован)";
