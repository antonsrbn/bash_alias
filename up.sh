#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
docker-compose down && docker-compose up -d && docker-compose ps && bash "$SCRIPT_DIR/cip.sh"
echo -e "\e[92mПроект перезапущен";
