#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

if [[ $(docker-compose ps &> /dev/null && echo "1" || echo "0") == '1' && $(docker-compose ps | grep $1 | wc -l) > 0 ]]; then
	docker-compose logs -f --tail=500 $1; 
else
 	docker logs -f --tail=500 $1;
fi