#!/bin/bash

if [[ $2 == '-u' ]]; then
	user=1000;
	if [[ $3 != '' ]]; then
		user=$3;
	fi	
	docker-compose run --entrypoint /bin/bash -u$user $1
else
	docker-compose run --entrypoint /bin/bash $1
fi
