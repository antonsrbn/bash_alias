<?php
$docRoot = $_SERVER['argv'][1];
$filePath = $_SERVER['argv'][2];
$content = file_get_contents($filePath);
$matches1 = [];
$matches2 = [];
preg_match_all("/[\"'][^(PATH('|\")\s{0,}+=>)](.+?inc\.php)'/", $content , $matches1);
preg_match_all("/[\"'][^(PATH('|\")\s{0,}+=>)](.+?inc\.php)\"/", $content , $matches2);

$matchesAll = array_merge($matches1[1] ?? [], $matches2[1]  ?? []);
foreach ($matchesAll as $path) {
	$valuex = explode('/', $path);
	$end = array_pop($valuex);
	$end = 'en-' . $end;
	$valuex[] = $end;
	$newPath = implode('/', $valuex);
	if(copy($docRoot . $path, $docRoot . $newPath)){
		$content = str_replace($path, $newPath, $content);
		echo $docRoot . $path . " - скопирован.\n";
	}
	else {
		echo $docRoot . $path . " - не удалось.\n";
	}
}
file_put_contents($filePath, $content);