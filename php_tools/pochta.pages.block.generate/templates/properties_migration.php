<?php

use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Loader;
use Pochtabank\Engine\Kernel\Migration\AbstractMigration;
use Pochtabank\Engine\Service\Helper\Migration\BlockMaker;

class AddProperty#PROPERTY_CODE_CS#For#ENTITY_API_CODE#Block extends AbstractMigration
{
    public function init(): void
    {
        parent::init();
        
        Loader::includeModule('iblock');
        Loader::includeModule('pochtabank.engine');
        
    }
    
    /**
     * @throws \Throwable
     */
    public function up(): void
    {
        BlockMaker::get()->transaction(function () {
            $id = BlockMaker::get()->getIblockIdByApiCode('#ENTITY_API_CODE#');
            
            #MigrationProperty#
        });
        
        
    }
    
    public function down(): void
    {
        $iblockId   = BlockMaker::get()->getIblockIdByApiCode('#ENTITY_API_CODE#');
        $properties = PropertyTable::query()
            ->addSelect('ID')
            ->whereIn('CODE', '#PROPERTY_CODE#')
            ->where('IBLOCK_ID', $iblockId)
            ->fetchCollection();
        
        foreach ($properties as $property) {
            CIBlockProperty::Delete($property->getId());
        }
    }
}
