/**
 * @throws ArgumentException
 * @throws JsonException
 * @throws ObjectPropertyException
 * @throws SystemException
 * @throws Exception
 */
private function make#METHOD_NAME#(): void
{
    $iblockApiCode = '#ENTITY_API_CODE#';

    $data = json_decode('#JSON#', true, 512, JSON_THROW_ON_ERROR);

    $sectionId = BlockMaker::get()->makeSection([
'API_CODE'          => $iblockApiCode,
'CODE'              => '#SECTION_CODE#',
'NAME'              => #SECTION_NAME#,
'IBLOCK_SECTION_ID' => null,
]);
#PREPARING#

BlockMaker::get()->makeElement([
'API_CODE' => $iblockApiCode,
'FIELDS'   => [
'IBLOCK_SECTION_ID' => $sectionId,
'NAME'              => #NAME#,
#PROPERTY_LIST#
],
]);
}

#AFTER_METHOD_CODE#
