<?php

namespace Pochtabank\Pages\Service\Block\#ENTITY_API_CODE#;

use Bitrix\Iblock\ORM\Query;
use Bitrix\Main\Error;
use Bitrix\Main\Result;
use Exception;
use Pochtabank\Pages\Dto\Block\Common\SectionList;
use Pochtabank\Pages\Service\Block\BasicBlockHandler;
use Pochtabank\Pages\Service\Block\Excluding;
use Pochtabank\Pages\Service\Block\SectionalBlockHandler;

/**
 * Class #ENTITY_API_CODE#Handler.
 */
class #ENTITY_API_CODE#Handler extends BasicBlockHandler implements SectionalBlockHandler, Excluding
{
    /**
     * @inheritDoc
     */
    public function modifyBlockData(array &$block, $pageFacade = null, $additionalFilterParams = null): void
    {
        $block['name']    = $block['name'] ?: '#ENTITY_API_CODE#';
        $block['section'] = (int)$block['section'] ?: 0;
    
        $filter = Query::filter()->where('IBLOCK_SECTION_ID', $block['section'] ?: null);
        $item   = (new #ENTITY_API_CODE#Repository($block['section'], $filter))->get();
    
        if ($item) {
            $block = array_merge($block, $item->toArray());
        }
    }
    
    /**
     * @inheritDoc
     */
    public function updateBlockData(array &$block, $pageFacade = null): Result
    {
        $result    = new Result();
        $sectionId = &$block['section'];
    
        try {
            if ($sectionId === null) {
                $sectionId  = #ENTITY_API_CODE#Repository::makeSection($block['sectionName'] ?: '')->id;
                $listResult = (new #ENTITY_API_CODE#Repository($sectionId))->add($block);
            } else {
                $filter     = Query::filter()->where('IBLOCK_SECTION_ID', $sectionId ?: null);
                $listResult = (new #ENTITY_API_CODE#Repository($sectionId, $filter))->update($block);
            }
        
            if (!$listResult->isSuccess()) {
                $result->addErrors($listResult->getErrors());
            }
        } catch (Exception $e) {
            $result->addError(new Error($e->getMessage()));
        }
    
        return $result;
    }
    
    /**
     * @inheritDoc
     */
    public function getExcludingFields(): array
    {
        return [
            'id',
            'code',
            'sort',
            'wfParentElementId',
#HandlerExcludingFields#
        ];
    }
    
    /**
     * @inheritDoc
     */
    public function getSections(): SectionList
    {
      return #ENTITY_API_CODE#Repository::getSections();
    }
}
