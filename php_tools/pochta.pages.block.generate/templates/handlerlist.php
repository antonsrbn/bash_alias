<?php

namespace Pochtabank\Pages\Service\Block\#ENTITY_API_CODE#;

use Bitrix\Iblock\ORM\Query;
use Bitrix\Main\Error;
use Bitrix\Main\Result;
use Exception;
use Pochtabank\Pages\Dto\Block\Common\SectionList;
use Pochtabank\Pages\Service\Block\BasicBlockHandler;
use Pochtabank\Pages\Service\Block\Excluding;
use Pochtabank\Pages\Service\Block\SectionalBlockHandler;

/**
 * Class #ENTITY_API_CODE#Handler.
 *
 * @package Pochtabank\Pages\Service\Block\#ENTITY_API_CODE#
 */
class #ENTITY_API_CODE#Handler extends BasicBlockHandler implements SectionalBlockHandler, Excluding
{
    /**
     * @inheritDoc
     */
    public function modifyBlockData(array &$block, $pageFacade = null, $additionalFilterParams = null): void
    {
        $block['name']    = $block['name'] ?? '#ENTITY_API_CODE#';
        $block['section'] = (int)$block['section'] ?: 0;
        $filter           = Query::filter()->where('IBLOCK_SECTION_ID', $block['section'] ?: null);
        $block['#HandlerExcludingFieldsList#']   = (new #ENTITY_API_CODE#Repository($block['section'], $filter))->get()->toArray();
    }
    
    /**
     * @inheritDoc
     */
    public function updateBlockData(array &$block, $pageFacade = null): Result
    {
        $result    = new Result();
        $sectionId = &$block['section'];
        $list      = $block ['#HandlerExcludingFieldsList#'] ?: [];
        try {
            if ($sectionId === null) {
                $sectionId  = #ENTITY_API_CODE#Repository::makeSection($block['sectionName'] ?: '')->id;
                $listResult = (new #ENTITY_API_CODE#Repository($sectionId))->add($list);
            } else {
                $filter     = Query::filter()->where('IBLOCK_SECTION_ID', $sectionId ?: null);
                $listResult = (new #ENTITY_API_CODE#Repository($sectionId, $filter))->update($list);
            }
            
            if (!$listResult->isSuccess()) {
                $result->addErrors($listResult->getErrors());
            }
        } catch (Exception $e) {
            $result->addError(new Error($e->getMessage()));
        }
        
        return $result;
    }
    
    /**
     * @inheritDoc
     */
    public function getSections(): SectionList
    {
        return #ENTITY_API_CODE#Repository::getSections();
    }
    
    /**
     * @inheritDoc
     */
    public function getExcludingFields(): array
    {
        return [
            '#HandlerExcludingFieldsList#'
        ];
    }
}
