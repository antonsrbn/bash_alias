<?php

namespace Pochtabank\Pages\Service\Block\#ENTITY_API_CODE#;

use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Objectify\EntityObject;
use CFile;
use Pochtabank\Engine\Service\File\File;
use Pochtabank\Engine\Orm\FileTable;
use Pochtabank\Engine\Service\Helper\StringUtils;
use Pochtabank\Pages\Dto\Block\BasicList;
use Pochtabank\Pages\Dto\Block\BasicListItem;
use Pochtabank\Pages\Orm\CollectionPicker;
use Pochtabank\Pages\Orm\Element\#ENTITY_API_CODE#Table;
use Pochtabank\Pages\Service\Block\BasicListRepository;
use Pochtabank\Engine\Orm\EO_Enum;

/**
 * Class #ENTITY_API_CODE#Repository.
 *
 * @package Pochtabank\Pages\Service\Block\#ENTITY_API_CODE#
 */
class #ENTITY_API_CODE#Repository extends BasicListRepository
{
    /**
     * @inheritDoc
     */
    protected static function getTableClass(): string
    {
       return #ENTITY_API_CODE#Table::class;
    }
    
    /**
     * @inheritDoc
     */
    protected static function getListClass(): string
    {
        return #ENTITY_API_CODE#List::class;
    }
    
    /**
     * @param #ENTITY_API_CODE#    $item
     * @param EntityObject|null $originalObject
     * @param bool              $active
     * @return EntityObject
     * @throws ArgumentException
     * @throws SystemException
     * @inheritDoc
     */
    protected function initObject(BasicListItem $item, ?EntityObject $originalObject = null, bool $active = true): EntityObject
    {
        $object = self::getTableClass()::createObject()
            ->setXmlId(uniqid())
            ->setName($item->get#RepositoryMainNameField#())
            ->setCode(StringUtils::translit($item->get#RepositoryMainNameField#()))
            ->setSort($item->getSort())
            ->setActive($active)
            ->setIblockSectionId($this->getSectionId() ?: null)
            ->setInSections($this->getSectionId() ? 'Y' : 'N');
    
    
#RepositoryInitObject#
    
        // Если передан оригинальный объект, то создаваемый тут явялется версией
        if (isset($originalObject)) {
            $object->setWfParentElementId($originalObject->getWfParentElementId() ?: $originalObject->getId());
            $object->setWfStatusId(READY);
        }
    
        return $object;
    }
    
    /**
     * @inheritDoc
     */
    protected function afterCreateObject(EntityObject $object, BasicListItem $item): void
    {
        $object->fill([
#RepositoryAfterCreateObject#
        ]);
    }
    
#RepositoryAfterCode#
}

