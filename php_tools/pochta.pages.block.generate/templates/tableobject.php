<?php

namespace Pochtabank\Pages\Orm\Element;

use Bitrix\Iblock\Elements\Element#ENTITY_API_CODE#Table;
use Bitrix\Iblock\Elements\EO_Element#ENTITY_API_CODE#;
use Bitrix\Iblock\Elements\EO_Element#ENTITY_API_CODE#_Entity;
use Bitrix\Iblock\ORM\ElementEntity;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\ORM\Objectify\EntityObject;
use Bitrix\Main\ORM\Query\Filter\ConditionTree;
use Bitrix\Main\SystemException;
use Pochtabank\Engine\Orm\Cacheable;
use Pochtabank\Engine\Orm\ElementTable;
use Pochtabank\Pages\Orm\ObjectPicker;

class #ENTITY_API_CODE#Table extends Element#ENTITY_API_CODE#Table implements ObjectPicker
{
    use Cacheable;

    /**
     * Получение сущности
     *
     * @return EO_Element#ENTITY_API_CODE#_Entity|ElementEntity
     * @throws ArgumentException
     * @throws SystemException
     */
    public static function getEntity(): ElementEntity
    {
        return Element#ENTITY_API_CODE#Table::getEntity();
    }

    /**
     * Получение объекта
     *
     * @param ConditionTree $filter
     *
     * @return EntityObject|EO_Element#ENTITY_API_CODE#|null
     * @throws ArgumentException
     * @throws SystemException
     * @throws ObjectPropertyException
     */
    public static function getObject(ConditionTree $filter): ?EntityObject
    {
        $items = ElementTable::getLastVersions(self::getEntity()->getIblock()->getApiCode(), CURRENT_STATUS);

        if (count($items) === 0) {
            return null;
        }

        return Element#ENTITY_API_CODE#Table::query()
            ->addSelect('*')
#TableQuery#
            ->whereIn('ID', array_column($items, 'MAX_ID'))
            ->where($filter)
            ->addOrder('SORT')
            ->setCacheTtl(self::getRandTtl())
            ->fetchObject();
    }
}

