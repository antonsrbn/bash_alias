<?php

namespace Pochtabank\Pages\Orm\Element;

use Bitrix\Iblock\ORM\ElementEntity;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\ORM\Objectify\Collection;
use Bitrix\Main\ORM\Query\Filter\ConditionTree;
use Bitrix\Main\SystemException;
use Pochtabank\Engine\Orm\Cacheable;
use Pochtabank\Engine\Orm\ElementTable;
use Pochtabank\Pages\Orm\CollectionPicker;
use Bitrix\Iblock\Elements\Element#ENTITY_API_CODE#Table;
use Bitrix\Iblock\Elements\EO_Element#ENTITY_API_CODE#_Collection;
use Bitrix\Iblock\Elements\EO_Element#ENTITY_API_CODE#_Entity;

class #ENTITY_API_CODE#Table extends Element#ENTITY_API_CODE#Table implements CollectionPicker
{
    
    use Cacheable;
    
    /**
     * Получение сущности
     *
     * @return EO_Element#ENTITY_API_CODE#_Entity|ElementEntity
     * @throws ArgumentException
     * @throws SystemException
     */
    public static function getEntity(): ElementEntity
    {
        return Element#ENTITY_API_CODE#Table::getEntity();
    }
    
    //#ENTITY_API_CODE#
    public static function getCollection(ConditionTree $filter): Collection
    {
        $items = ElementTable::getLastVersions(self::getEntity()->getIblock()->getApiCode(), CURRENT_STATUS);
        
        if (count($items) === 0) {
            return Element#ENTITY_API_CODE#Table::createCollection();
        }
        return Element#ENTITY_API_CODE#Table::query()
            ->addSelect('*')
#TableQuery#
            ->whereIn('ID', array_column($items, 'MAX_ID'))
            ->where($filter)
            ->addOrder('SORT')
            ->setLimit(20)
            ->setCacheTtl(self::getRandTtl())
            ->fetchCollection();
        
    }
    
    
}

