<?php

namespace Pochtabank\Pages\Service\Block\#ENTITY_API_CODE#;

use Pochtabank\Pages\Dto\Block\BasicList;

/**
 * Class #ENTITY_API_CODE#List.
 *
 * @package Pochtabank\Pages\Dto\Block\#ENTITY_API_CODE#
 */
class #ENTITY_API_CODE#List extends BasicList
{
    /**
     * @inheritDoc
     */
    protected static function getItemClassName(): string
    {
        return #ENTITY_API_CODE#::class;
    }
}
