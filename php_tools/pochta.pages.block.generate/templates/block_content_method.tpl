/**
 * @throws ArgumentException
 * @throws JsonException
 * @throws ObjectPropertyException
 * @throws SystemException
 * @throws Exception
 */
private function make#METHOD_NAME#(): void
{
#METHOD_BODY#
}

#AFTER_METHOD_CODE#
