<?php

namespace Pochtabank\Pages\Service\Block\#ENTITY_API_CODE#;

use Bitrix\Main\ORM\Objectify\EntityObject;
use InvalidArgumentException;
use Pochtabank\Engine\Service\File\File;
use Pochtabank\Engine\Service\Helper\JsonField;
use Pochtabank\Pages\Dto\Block\BasicListItem;
use Bitrix\Iblock\Elements\EO_Element#ENTITY_API_CODE#;

/**
 * Class #ENTITY_API_CODE#.
 *
 * @package Pochtabank\Pages\Dto\Block\#ENTITY_API_CODE#
 */
class #ENTITY_API_CODE# extends BasicListItem
{
    use JsonField;
    
#DtoProperty#
    
    /**
     * @inheritDoc
     */
    public static function fromArray(array $data): self
    {
        return parent::fromArray($data)
#DtoFromArray#
            ;
    }
    
    public static function fromObject(EntityObject $object): self
    {
        $dto = parent::fromObject($object);
        
        if (!$object instanceof EO_Element#ENTITY_API_CODE#) {
            throw new InvalidArgumentException(sprintf(
                'Invalid object type. Expected %s, but received %s',
                EO_Element#ENTITY_API_CODE#::class,
                get_class($object)
            ));
        }
    
#DtoFromObject#
        
        return $dto
#DtoFromObjectDto#
            ;
    }
    
    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return array_merge(parent::toArray(), [
#DtoToArray#
        ]);
    }
    
#DtoGetter#

#DtoSetter#
}
