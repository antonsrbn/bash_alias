<?php

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\ORM\Objectify\EntityObject;
use Bitrix\Main\SystemException;
use Pochtabank\Engine\Kernel\Migration\AbstractMigration;
use Pochtabank\Engine\Service\Helper\Migration\BlockMaker;

class Add#ROUTE#Page extends AbstractMigration
{
    /**
     * @throws LoaderException
     */
    public function init(): void
    {
        parent::init();
        Loader::includeModule('pochtabank.engine');
        Loader::includeModule('iblock');
    }
    
    /**
     * @throws Throwable
     */
    public function up(): void
    {
        BlockMaker::get()->transaction(function () {
            $iblockApiCode   = 'Structure';
            $parentSectionId = null;
            
            if (#HAS_PARENT_ROUTE#) {
            $parentSectionId = BlockMaker::get()->getSectionId(
                $iblockApiCode,
                '#ROUTE_PARENT#'
            );
        }
		          
            $sectionId = BlockMaker::get()->makeOrGetSection([
                'API_CODE'          => $iblockApiCode,
                'CODE'              => '#ROUTE_END#',
                'NAME'              => '#SECTION_NAME#',
                'IBLOCK_SECTION_ID' => $parentSectionId,
            ]);
            
            $headerData = <<<HEADER
  #HEADER_JSON#
HEADER;
            
            $breadcrumbsData = <<<BREADCRUMBS
#BREADCRUMBS_JSON#
BREADCRUMBS;
            
            $metaData = <<<META
#META_JSON#
META;
            
            $footerData = <<<FOOTER
#FOOTER_JSON#
FOOTER;
            $blocks = [];
            
            #BLOCKS_LIST#
           
            
            $sidebarData = '#SIDEBAR_JSON#';
            $callSeo     = function (int $id) {
                $this->setSeo($id);
            };
            $callSeo->bindTo($this);
            
            BlockMaker::get()->makeElement([
                'API_CODE' => $iblockApiCode,
                'FIELDS'   => [
                    'IBLOCK_SECTION_ID' => $sectionId,
                    'NAME'              => 'Главная страница',
                    'CODE'              => 'index',
                    'BLOCKS'            => BlockMaker::get()->arrayToJsonToHtmlText($blocks),
                    'HEADER'            => BlockMaker::get()->jsonToHtmlText($headerData),
                    'FOOTER'            => BlockMaker::get()->jsonToHtmlText($footerData),
                    'BREADCRUMBS'       => BlockMaker::get()->jsonToHtmlText($breadcrumbsData),
                    'META'              => BlockMaker::get()->jsonToHtmlText($metaData),
                    'SIDEBAR'           => BlockMaker::get()->jsonToHtmlText($sidebarData),
                    #MORE_PROPERY_VALUES#
                    'ASSIGNMENT'        => 'index_page',
                ],
            ], true, $callSeo);
        }, $this);
    }

    protected function setSeo(int $id): void
    {
        $el                 = new CIBlockElement();
        $arLoadProductArray = [
            'IPROPERTY_TEMPLATES' => [
                'ELEMENT_META_TITLE'       => '#SEO_TITLE#',
                'ELEMENT_META_DESCRIPTION' => '#SEO_DESCRIPTION#',
                'ELEMENT_META_KEYWORDS'    => '#SEO_KEYWORDS#',
            ],
        ];
        
        $PRODUCT_ID = $id;
        $el->Update($PRODUCT_ID, $arLoadProductArray);
    }

    /**
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function down(): void
    {
        BlockMaker::get()->removeSectionByCodePath('Structure', '#REMOVE_ROUTE_PATH#');
    }
}
