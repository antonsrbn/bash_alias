<?php

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;
use Pochtabank\Engine\Kernel\Migration\AbstractMigration;
use Pochtabank\Engine\Service\Helper\Migration\BlockMaker;

class CreateIblock#ENTITY_API_CODE# extends AbstractMigration
{
    public function init(): void
    {
        parent::init();
        
        Loader::includeModule('iblock');
        Loader::includeModule('pochtabank.engine');
        
    }
    
    /**
     * @throws \Throwable
     */
    public function up(): void
    {
        BlockMaker::get()->transaction(function () {
            
            
            $iblock = new CIBlock();
            $id     = $iblock->Add([
                'CODE'           => '#ENTITY_CODE#',
                'API_CODE'       => '#ENTITY_API_CODE#',
                'IBLOCK_TYPE_ID' => '#ENTITY_API_CODE_TYPE#',
                'NAME'           => '#ENTITY_NAME#',
                'SITE_ID'        => ['s1'],
                'GROUP_ID'       => ['1' => 'X', '2' => 'R'],
                'FIELDS'         => [
                    'CODE'         => [
                        'IS_REQUIRED'   => 'Y',
                        'DEFAULT_VALUE' => [
                            'UNIQUE'          => 'N',
                            'TRANSLITERATION' => 'Y',
                            'TRANS_LEN'       => '100',
                            'TRANS_CASE'      => 'L',
                            'TRANS_SPACE'     => '-',
                            'TRANS_OTHER'     => '-',
                            'TRANS_EAT'       => 'Y',
                            'USE_GOOGLE'      => 'N',
                        ],
                    ],
                    'SECTION_CODE' => [
                        'IS_REQUIRED'   => 'Y',
                        'DEFAULT_VALUE' => [
                            'UNIQUE'          => 'N',
                            'TRANSLITERATION' => 'Y',
                            'TRANS_LEN'       => '100',
                            'TRANS_CASE'      => 'L',
                            'TRANS_SPACE'     => '-',
                            'TRANS_OTHER'     => '-',
                            'TRANS_EAT'       => 'Y',
                            'USE_GOOGLE'      => 'N',
                        ],
                    ],
                ],
            ]);
            #MigrationProperty#
        });
        
        
    }
    
    public function down(): void
    {
        $iblock = IblockTable::query()
            ->where('API_CODE', '#ENTITY_API_CODE#')
            ->fetchObject();
        
        if ($iblock === null) {
            return;
        }
        
        CIBlock::Delete($iblock->getId());
    }
}
