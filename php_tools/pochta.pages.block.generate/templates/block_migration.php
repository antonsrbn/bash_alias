<?php

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Pochtabank\Engine\Kernel\Migration\AbstractMigration;
use Pochtabank\Engine\Service\Helper\Migration\BlockMaker;

class Add#ROUTE#Blocks extends AbstractMigration
{
    /**
     * @throws LoaderException
     */
    public function init(): void
    {
        parent::init();
        
        Loader::includeModule('pochtabank.engine');
    }
    
    /**
     * @throws Throwable
     */
    public function up(): void
    {
        BlockMaker::get()->transaction(
            function () {
                #METHODS_UP#
            }, $this);
    }
    
    /**
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function down(): void
    {
        #METHODS_DOWN#
    }
    
    #BLOCKS#
}
