<?php

namespace Tools;

use RuntimeException;
use Symfony\Component\VarDumper\VarDumper;
use Tools\Detectors\DTO\TinaBlock;
use Tools\Detectors\DTO\TinaProp;
use Tools\FieldsFactory\BaseField;
use Tools\FieldsFactory\BooleanField;
use Tools\FieldsFactory\EnumField;
use Tools\FieldsFactory\FileField;
use Tools\FieldsFactory\IField;
use Tools\FieldsFactory\JsonField;
use Tools\FieldsFactory\StringField;
use Tools\FieldsFactory\TextField;

/**
 * Class FieldParser.
 */
class FieldParser
{
    private const FIELD_FACTORY = [
        BooleanField::class,
        EnumField::class,
        JsonField::class,
        TextField::class,
        FileField::class,
        StringField::class,
    ];
    /**
     * @var array
     */
    private $data;
    /**
     * @var bool
     */
    private $hasList;
    /**
     * @var string|null
     */
    private $listField;
    
    /**
     * @var TinaBlock
     */
    private $tinaBlock;
    
    
    public function __construct()
    {
        $this->data    = [
            'repositoryInitObject'        => [],
            'repositoryAfterCreateObject' => [],
            'dtoProperty'                 => [],
            'dtoFromArray'                => [],
            'dtoFromObject'               => [],
            'dtoFromObjectDto'            => [],
            'dtoToArray'                  => [],
            'dtoGetter'                   => [],
            'dtoSetter'                   => [],
            'handlerExcludingFields'      => [],
            'tableQuery'                  => [],
            'repositoryAfterCode'         => [],
            'migrationProperty'           => [],
        ];
        $this->hasList = false;
    }
    
    public function setHasList(bool $hasList): self
    {
        $this->hasList = $hasList;
        
        return $this;
    }
    
    public function setListField(?string $listField): self
    {
        $this->listField = $listField;
        
        return $this;
    }
    
    public static function compareTypes(IField $typeOne, IField $typeTwo): IField
    {
        return self::getPriorityType($typeOne) < self::getPriorityType($typeTwo) ? $typeOne : $typeTwo;
    }
    
    private static function getPriorityType(IField $type): ?int
    {
        $priority = null;
        foreach (self::FIELD_FACTORY as $key => $item) {
            if (get_class($type) === $item) {
                $priority = $key;
                break;
            }
        }
        
        return $priority;
    }
    
    /**
     * @param array      $data
     * @param string     $blockName
     * @param array|null $configureTypes
     * @return array
     */
    public function parse(array $data, string $blockName, ?array $configureTypes = null): array
    {
        $this->tinaBlock = Storage::getTinaBlocks()[$blockName];
        
        foreach ($data as $key => $value) {
            if ($key === '_template') {
                continue;
            }
            
            if ($configureTypes !== null && array_key_exists($key, $configureTypes)) {
                $field = $configureTypes[$key];
            } else {
                $field = $this->makeField($key, $value);
            }
            
            if ($field->makeForRepositoryInitObject() !== null) {
                $this->data['repositoryInitObject'][] = $field->makeForRepositoryInitObject();
            }
            if ($field->makeForRepositoryAfterCreateObject() !== null) {
                $this->data['repositoryAfterCreateObject'][] = $field->makeForRepositoryAfterCreateObject();
            }
            if ($field->makeForDtoProperty() !== null) {
                $this->data['dtoProperty'][] = $field->makeForDtoProperty();
            }
            if ($field->makeForDtoFromArray() !== null) {
                $this->data['dtoFromArray'][] = $field->makeForDtoFromArray();
            }
            if ($field->makeForDtoFromObject() !== null) {
                $this->data['dtoFromObject'][] = $field->makeForDtoFromObject();
            }
            if ($field->makeForDtoFromObjectDto() !== null) {
                $this->data['dtoFromObjectDto'][] = $field->makeForDtoFromObjectDto();
            }
            if ($field->makeForDtoToArray() !== null) {
                $this->data['dtoToArray'][] = $field->makeForDtoToArray();
            }
            if ($field->makeForDtoGetter() !== null) {
                $this->data['dtoGetter'][] = $field->makeForDtoGetter();
            }
            if ($field->makeForDtoSetter() !== null) {
                $this->data['dtoSetter'][] = $field->makeForDtoSetter();
            }
            if ($field->makeForHandlerExcludingFields() !== null) {
                $this->data['handlerExcludingFields'][] = $field->makeForHandlerExcludingFields();
            }
            if ($field->makeForTableQuery() !== null) {
                $this->data['tableQuery'][] = $field->makeForTableQuery();
            }
            if ($field->makeForRepositoryAfterCode() !== null) {
                $this->data['repositoryAfterCode'][] = $field->makeForRepositoryAfterCode();
            }
            if ($field->makeMigrationProperty() !== null) {
                $this->data['migrationProperty'][] = $field->makeMigrationProperty();
            }
        }
        
        return $this->data;
    }
    
    public function getFieldTypes(array $data, string $blockName): array
    {
        $this->tinaBlock = Storage::getTinaBlocks()[$blockName];
        
        $allFields = [];
        
        foreach ($data as $key => $value) {
            if ($key === '_template') {
                continue;
            }
            
            $field           = $this->makeField($key, $value);
            $allFields[$key] = $field;
        }
        
        return $allFields;
    }
    
    public function getMainNameFieldName(array $data, string $blockName): string
    {
        $this->tinaBlock = Storage::getTinaBlocks()[$blockName];
        
        $fields = [];
        foreach ($data as $key => $value) {
            if ($key === '_template') {
                continue;
            }
            
            $field = $this->makeField($key, $value);
            if ($field instanceof StringField || $field instanceof TextField) {
                $fields[$key] = $field;
            }
        }
        
        foreach ($fields as $key => $field) {
            if ($field instanceof StringField && $key === 'name') {
                return ucfirst($key);
            }
        }
        
        foreach ($fields as $key => $field) {
            if ($field instanceof StringField && $key === 'title') {
                return ucfirst($key);
            }
        }
        
        foreach ($fields as $key => $field) {
            if ($field instanceof StringField) {
                return ucfirst($key);
            }
        }
        
        foreach ($fields as $key => $field) {
            if ($field instanceof TextField) {
                return ucfirst($key);
            }
        }
        
        $key = array_keys($data)[0];
        
        if ($key === '_template') {
 
            throw new RuntimeException('fuck!!');
        }
        
        return ucfirst($key);
    }
    
    /**
     * @param string $key
     * @param        $value
     * @return IField
     */
    private function makeField(string $key, $value): IField
    {
        //     VarDumper::dump([$key,$value]);
        $prop = $this->getTinaProp($key);
        
        $title = $prop ? $prop->getTitle() : null;
        
        if ($prop && $prop->getTypeClassName()) {
            $className = $prop->getTypeClassName();
            
            return new $className($key, $value, $title);
        }
        
        /**
         * @var $className IField
         */
        foreach (self::FIELD_FACTORY as $className) {
            if ($className::hasCondition($key, $value)) {
                return new $className($key, $value, $title);
            }
        }
        
        return new BaseField($key, $value, $title);
    }
    
    private function getTinaProp(string $key): ?TinaProp
    {
        if ($this->hasList) {
            $listProp = $this->tinaBlock->getProps()[$this->listField] ?? null;
            return $listProp ? ($listProp->getProps()[$key] ?? null) : null;
        }
        return $this->tinaBlock->getProps()[$key] ?? null;
    }
}
