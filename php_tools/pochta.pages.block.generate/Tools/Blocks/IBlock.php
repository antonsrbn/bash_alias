<?php

namespace Tools\Blocks;

use Tools\FieldsFactory\IField;

interface IBlock
{
    public function __construct(array $blockData, string $elementVariable, array $fieldTypes);
    
    public static function getName(): string;
    
    public static function hasOnlyTemplateMethod(): bool;
    
    public static function hasOriginalBlockData(): bool;
    
    public static function hasList(): ?bool;
    
    public static function getSectionCode(string $route, ?string $worldNumber): string;
    
    public static function getListField(): ?string;
    
    public function getDataInStructure(
        array $originalData,
        bool $hasList,
        string $apiCode,
        string $sectionCode,
        ?string $iterableField = null
    ): string;
    
    public function getGenerateCode(): string;
    
    public function detectingFields(IBlockContentDetector $blockContentDetector, array $blocksData, string $apiCode): array;
    
    public function getGenerateAfterMethodCode(): string;
    
    public function getRemoveCode(string $route, string $apiCode, ?string $worldNumber): string;
}
