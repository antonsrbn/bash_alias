<?php

namespace Tools\Blocks;

interface IContextOperator
{
    public function setContext(BlocksParser $blocksParser):void;
}
