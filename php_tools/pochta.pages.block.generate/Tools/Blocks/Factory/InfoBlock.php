<?php

namespace Tools\Blocks\Factory;

use Tools\Blocks\BaseBlock;
use Tools\FieldsFactory\StringField;

/**
 * Class InfoBlock.
 *
 * @package Tools\Blocks\Factory
 */
class InfoBlock extends BaseBlock
{
    public function __construct(array $blockData, string $elementVariable, ?array $fieldTypes = null)
    {
        parent::__construct($blockData, $elementVariable, $fieldTypes);
        
        if (array_key_exists('subtitle', $this->fieldTypes)) {
            $this->fieldTypes['subtitle'] = $this->fieldTypes['subtitle']->makeOtherTypeField(StringField::class);
        }
    }
    
    public static function getName(): string
    {
        return 'info';
    }
    
    public static function hasList(): ?bool
    {
        return true;
    }
}
