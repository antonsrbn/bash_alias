<?php

namespace Tools\Blocks\Factory;

use Tools\Blocks\BaseBlock;
use Tools\FieldsFactory\TextField;

/**
 * Class QuestionsBlock.
 *
 * @package Tools\Blocks\Factory
 */
class QuestionsBlock extends BaseBlock
{
    public function __construct(array $blockData, string $elementVariable, ?array $fieldTypes = null)
    {
        parent::__construct($blockData, $elementVariable, $fieldTypes);
        
        if (array_key_exists('desc', $this->fieldTypes)) {
            $this->fieldTypes['desc'] = $this->fieldTypes['desc']->makeOtherTypeField(TextField::class);
        }
    }
    
    public static function getName(): string
    {
        return 'questions';
    }
    
    public static function hasList(): ?bool
    {
        return true;
    }
}
