<?php

namespace Tools\Blocks\Factory;

use Tools\Blocks\BaseBlock;
use Tools\FieldParser;
use Tools\FieldsFactory\StringField;
use Tools\NameTranslator;

class PromoBlock extends BaseBlock
{
    public static function getName(): string
    {
        return 'promo';
    }
    
    public static function hasList(): ?bool
    {
        return false;
    }
    
    public function getGenerateCode(): string
    {
        $fieldParser = new FieldParser();
        
        $data = $this->blockData['banner'];
        
        $types = $fieldParser->getFieldTypes($data, $this->blockData['_template']);
        
        if (array_key_exists('description', $types)) {
            $types['description'] = new StringField('description', $data['description']);
        }
        
        if (array_key_exists('background', $types)) {
            $types['background'] = new StringField('background', $data['background']);
        }
        
        $contentFields = $this->makeFields($data, $this->elementVariable . '[\'banner\']', $types);
        
        unset($this->blockData['banner'], $this->fieldTypes['banner']);
        
        $contentFields .= "\n" . $this->makeFields($this->blockData, $this->elementVariable, $this->fieldTypes);
      
        return $contentFields;
    }
    
    private function makeFields(array $data, string $elementVariable, array $types): string
    {
        $contentFields = [];
        
        foreach ($data as $key => $blockDatum) {
            if ($key === '_template') {
                continue;
            }
            
            if ($key === 'name') {
                continue;
            }
            
            $translator  = new NameTranslator($key);
            $makeContent = $types[$key]->makeForBlockMigration();
            
            $makeContent = str_replace('#FIELD#', "\$$elementVariable" . "['$key']", $makeContent);
            
            $contentFields[] = "\t\t" . "'{$translator->getColumnName()}' => $makeContent,";
        }
        
        return implode("\n", $contentFields);
    }
}
