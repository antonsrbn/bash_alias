<?php

namespace Tools\Blocks\Factory;

use Tools\Blocks\BaseBlock;
use Tools\FieldsFactory\TextField;
use Tools\NameTranslator;

/**
 * Class InfoBgBlockBlock.
 *
 * @package Tools\Blocks\Factory
 */
class InfoBgBlockBlock extends BaseBlock
{
    public function __construct(array $blockData, string $elementVariable, ?array $fieldTypes = null)
    {
        parent::__construct($blockData, $elementVariable, $fieldTypes);
        
        if (array_key_exists('desc', $this->fieldTypes)) {
            $this->fieldTypes['desc'] = $this->fieldTypes['desc']->makeOtherTypeField(TextField::class);
        }
    }
    
    public static function getName(): string
    {
        return 'infoBgBlock';
    }
    
    public static function hasList(): ?bool
    {
        return false;
    }
    
    
    protected function makeRow(NameTranslator $translator, string $makeContent): string
    {
        $replaceList = [
            'LINK'      => 'BTN_LINK',
            'LINK_NAME' => 'BTN_LINK_NAME',
        ];
        
        $name = $replaceList[$translator->getColumnName()] ?? $translator->getColumnName();
        
        return "\t\t" . "'$name' => $makeContent,";
    }
}
