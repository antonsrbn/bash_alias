<?php

namespace Tools\Blocks\Factory;

use Tools\Blocks\BaseBlock;
use Tools\Blocks\BlocksParser;
use Tools\Blocks\IContextOperator;

class TabsBlock extends BaseBlock implements IContextOperator
{
    /**
     * @var array
     */
    private $blocksContent;
    
    public function __construct(array $blockData, string $elementVariable, ?array $fieldTypes = null)
    {
        parent::__construct($blockData, $elementVariable, $fieldTypes);
        
        $this->blocksContent = [];
    }
    
    public static function getName(): string
    {
        return 'tabs';
    }
    
    public static function hasOnlyTemplateMethod(): bool
    {
        return true;
    }
    
    public static function hasOriginalBlockData(): bool
    {
        return true;
    }
    
    public function getGenerateCode(): string
    {
        return implode("\n", $this->blocksContent['blockContentUpList']);
    }
    
    public function getPrepareGenerateCode(): string
    {
        return '';
    }
    
    public function getGenerateAfterMethodCode(): string
    {
        return implode("\n", $this->blocksContent['blockContentList']);
    }
    
    public function getRemoveCode(string $route, string $apiCode, ?string $worldNumber): string
    {
        return implode("\n", $this->blocksContent['removeBlockContentList']);
    }
    
    public function setContext(BlocksParser $blocksParser): void
    {
        $data = $this->blockData;
        
        $title = $data['name'] ?? $data['title'];
        
        $blocks = [];
        foreach ($data['tabs'] as $tab) {
            foreach ($tab['blocks'] as $item) {
                $potentialName           = ($tab['tabTitle'] ?: $title) . ' для страницы "' . $blocksParser->getTitlePage() . '"';
                $item['__potentialName'] = $potentialName;
                
                $blocks[] = $item;
            }
            
        }
        
        $context = $blocksParser->selfClone($blocksParser, $blocks);
        
        $this->blocksContent = $context->getContentMakeBlocks();
    }
    
    
    public function getDataInStructure(
        array $originalData,
        bool $hasList,
        string $apiCode,
        string $sectionCode,
        ?string $iterableField = null
    ): string {
        $template = $originalData['_template'];
        
        $blockNumber = 1;
        foreach ($originalData['tabs'] as &$tab) {
            foreach ($tab['blocks'] as &$block) {
                $block = '#TAB_BLOCK_' . $blockNumber . '#';
                $blockNumber++;
            }
            unset($block);
        }
        unset($tab);
        
        $originalData['_template'] = $template;
        
        $json = json_encode($originalData, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE);
        
        foreach ($this->blocksContent['dataInStructureList'] as $key => $contentBlock) {
            $number = $key + 1;
            $json   = str_replace('"#TAB_BLOCK_' . $number . '#"', $contentBlock, $json);
        }
        
        return $json;
    }
}
