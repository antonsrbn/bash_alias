<?php

namespace Tools\Blocks\Factory;

use Tools\Blocks\BaseBlock;
use Tools\FieldsFactory\EnumField;

/**
 * Class AdvantagesBlock.
 *
 * @package Tools\Blocks\Factory
 */
class AdvantagesBlock extends BaseBlock
{
    public function __construct(array $blockData, string $elementVariable, ?array $fieldTypes = null)
    {
        parent::__construct($blockData, $elementVariable, $fieldTypes);
        
        if (array_key_exists('size', $this->fieldTypes)) {
            $this->fieldTypes['size'] = $this->fieldTypes['size']->makeOtherTypeField(EnumField::class);
        }
    }
    
    public static function getName(): string
    {
        return 'advantages';
    }
    
    public static function hasList(): ?bool
    {
        return true;
    }
}
