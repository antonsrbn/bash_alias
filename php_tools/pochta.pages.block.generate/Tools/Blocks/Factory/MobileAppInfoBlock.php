<?php

namespace Tools\Blocks\Factory;

use Tools\Blocks\BaseBlock;
use Tools\FieldsFactory\StringField;

class MobileAppInfoBlock extends BaseBlock
{
    public function __construct(array $blockData, string $elementVariable, ?array $fieldTypes = null)
    {
        parent::__construct($blockData, $elementVariable, $fieldTypes);
        
        if (array_key_exists('text', $this->fieldTypes)) {
            $this->fieldTypes['text'] = $this->fieldTypes['text']->makeOtherTypeField(StringField::class);
        }
    }
    
    public static function getName(): string
    {
        return 'mobileAppInfo';
    }
    
    public static function hasList(): ?bool
    {
        return false;
    }
}
