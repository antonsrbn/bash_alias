<?php

namespace Tools\Blocks;

use Tools\FieldsFactory\IField;
use Tools\Helper;
use Tools\NameTranslator;
use Tools\Storage;

/**
 * Class MigrationPropertyMaker.
 *
 * @package Tools\Blocks
 */
class MigrationPropertyMaker
{
    /**
     * @var array
     */
    private $data;
    /**
     * @var string
     */
    private $templatePath;
    /**
     * @var string
     */
    private $absolutePath;
    /**
     * @var string
     */
    private $migrationsPath;
    /**
     * @var int
     */
    private $datePrefix;
    /**
     * @var array
     */
    private $log;
    
    public function __construct(
        array $data,
        string $templatePath,
        string $absolutePath,
        string $migrationsPath,
        int $datePrefix
    ) {
        $this->data           = $data;
        $this->templatePath   = $templatePath;
        $this->absolutePath   = $absolutePath;
        $this->migrationsPath = $migrationsPath;
        $this->datePrefix     = $datePrefix;
        $this->log            = [];
    }
    
    public function generate(): self
    {
        $this->log = [];
        
        $templateContent = file_get_contents($this->templatePath . '/' . 'properties_migration.php');
        
        foreach ($this->data as $entityApiCode => $fields) {
            /**
             * @var $field IField
             */
            foreach ($fields as $key => $field) {
                $filePathProperty = $this->absolutePath
                    . '/'
                    . $this->migrationsPath
                    . '/'
                    . ($this->datePrefix + Storage::tick())
                    . '_add_property_' . (new NameTranslator($key))->getFileCodeName() . '_for_'
                    . (new NameTranslator($entityApiCode))->getFileCodeName()
                    . '_block.php';
                $translator       = new NameTranslator($key);
                $content          = str_replace([
                    '#MigrationProperty#',
                    '#PROPERTY_CODE#',
                    '#PROPERTY_CODE_CS#',
                    '#ENTITY_API_CODE#',
                ], [
                    $field->makeMigrationProperty(),
                    $translator->getColumnName(),
                    $translator->getForMethodName(),
                    $entityApiCode,
                ], $templateContent);
                
                if (file_exists($filePathProperty)) {
                    $this->log[$entityApiCode][$key] = "$filePathProperty уже существует";
                } else {
                    Helper::makeDirectories($this->absolutePath . '/' . $this->migrationsPath);
                    
                    $result = file_put_contents($filePathProperty, $content);
                    if ($result) {
                        $this->log[$entityApiCode][$key] = "$filePathProperty создан\n";
                    } else {
                        $this->log[$entityApiCode][$key] = "$filePathProperty не смог быть создан\n";
                    }
                }
                
            }
            
        }
        
        return $this;
    }
    
    /**
     * @return array
     */
    public
    function getLog(): array
    {
        return $this->log;
    }
}
