<?php

namespace Tools\Blocks;

class PageMaker
{
    /**
     * @var array
     */
    private $data;
    /**
     * @var array
     */
    private $blocksForStructure;
    /**
     * @var string
     */
    private $templatePath;
    /**
     * @var string
     */
    private $route;
    
    public function __construct(
        array $data,
        array $blocksForStructure,
        string $templatePath,
        string $route
    ) {
        $this->data               = $data;
        $this->blocksForStructure = $blocksForStructure;
        $this->templatePath       = $templatePath;
        $this->route              = $route;
    }
    
    public function makePageContent(): string
    {
        $pageContent = file_get_contents($this->templatePath . '/' . 'page_migration.php');
        $blocksList  = implode("\n", $this->blocksForStructure);
        
        $route = preg_replace_callback('/[_\-\/](.)/', static function ($matches) {
            return ucfirst($matches[1]);
        }, $this->route);
        
        $route = ucfirst($route);
        
        $routeArray = explode('/', $this->route);
        
        $routeEnd = array_pop($routeArray);
        
        $routeParent = implode('/', $routeArray);
        
        $meta = $this->data['meta'];
        
        $sideBar = '{
    "active": false,
    "blocks": []
  }';
        
        unset($meta['title'], $meta['description'], $meta['keywords']);
        
        return str_replace([
            '#ROUTE#',
            '#BLOCKS_LIST#',
            '#REMOVE_ROUTE_PATH#',
            '#HAS_PARENT_ROUTE#',
            '#ROUTE_PARENT#',
            '#ROUTE_END#',
            '#SECTION_NAME#',
            '#HEADER_JSON#',
            '#BREADCRUMBS_JSON#',
            '#META_JSON#',
            '#FOOTER_JSON#',
            '#SIDEBAR_JSON#',
            '#SEO_TITLE#',
            '#SEO_DESCRIPTION#',
            '#SEO_KEYWORDS#',
            '#MORE_PROPERY_VALUES#'
        ], [
            $route,
            $blocksList,
            $this->route,
            $routeArray ? 'true' : 'false',
            $routeParent,
            $routeEnd,
            $this->data['meta']['title'],
            $this->json($this->data['header']),
            $this->json($this->data['breadcrumbs']),
            $this->json($meta),
            $this->json($this->data['footer']),
            $sideBar,
            $this->data['meta']['title'],
            $this->data['meta']['description'],
            $this->data['meta']['keywords'],
            '',
        
        ], $pageContent);
    }
    
    private function json(array $data): string
    {
        return json_encode($data, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE);
    }
}
