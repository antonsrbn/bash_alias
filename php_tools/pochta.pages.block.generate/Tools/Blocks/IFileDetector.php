<?php

namespace Tools\Blocks;

use Tools\Detectors\BlockDetected;
use Tools\ISimpleFileDetector;

interface IFileDetector extends ISimpleFileDetector
{
    public function context(): BlockDetected;
}
