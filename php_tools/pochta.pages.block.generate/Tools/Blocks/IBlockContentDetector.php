<?php

namespace Tools\Blocks;

interface IBlockContentDetector
{
    public function getRepositoryContent(): ?IFileDetector;
    
    public function getTableContent(): ?IFileDetector;
    
    public function getDtoContent(): ?IFileDetector;
}
