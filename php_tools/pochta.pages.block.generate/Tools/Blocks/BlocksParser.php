<?php

namespace Tools\Blocks;

use Exception;
use NumberToWords\Locale\English;
use NumberToWords\NumberToWords;
use Tools\Blocks\DTO\MigrationData;
use Tools\Blocks\Factory\AdvantagesBlock;
use Tools\Blocks\Factory\InfoBgBlockBlock;
use Tools\Blocks\Factory\InfoBlock;
use Tools\Blocks\Factory\MobileAppInfoBlock;
use Tools\Blocks\Factory\PromoBlock;
use Tools\Blocks\Factory\QuestionsBlock;
use Tools\Blocks\Factory\TabsBlock;
use Tools\ContentReplacer;
use Tools\Detectors\BlockDetector;
use Tools\Detectors\BlockTypeDetector;
use Tools\FieldParser;
use Tools\IPrototype;

class BlocksParser implements IPrototype
{
    private const BLOCKS = [
        TabsBlock::class,
        PromoBlock::class,
        MobileAppInfoBlock::class,
        AdvantagesBlock::class,
        InfoBgBlockBlock::class,
        InfoBlock::class,
        QuestionsBlock::class,
    ];
    /**
     * @var array
     */
    private $data;
    /**
     * @var string
     */
    private $templatePath;
    /**
     * @var string
     */
    private $route;
    /**
     * @var string
     */
    private $titlePage;
    /**
     * @var NumberToWords
     */
    private $numberWorlder;
    /**
     * @var BlockDetector
     */
    private $blockDetector;
    /**
     * @var array
     */
    private $blockInvited;
    
    public function __construct(
        array $data,
        string $templatePath,
        string $route,
        string $titlePage,
        BlockDetector $blockDetector
    ) {
        $this->data          = $data;
        $this->templatePath  = $templatePath;
        $this->route         = $route;
        $this->numberWorlder = new NumberToWords(new English());
        $this->titlePage     = $titlePage;
        $this->blockDetector = $blockDetector;
    }
    
    public function makeBlocksContent(): MigrationData
    {
        $contentMakeBlocks = $this->getContentMakeBlocks();
        
        $migrationContent = file_get_contents($this->templatePath . '/block_migration.php');
        
        $route = preg_replace_callback('/[_\-\/](.)/', static function ($matches) {
            return ucfirst($matches[1]);
        }, $this->route);
        
        $route = ucfirst($route);
        
        $addBLocksContent = str_replace([
            '#ROUTE#',
            '#METHODS_UP#',
            '#METHODS_DOWN#',
            '#BLOCKS#',
        ], [
            $route,
            implode("\n", $contentMakeBlocks['blockContentUpList']),
            implode("\n", $contentMakeBlocks['removeBlockContentList']),
            implode("\n", $contentMakeBlocks['blockContentList']),
        ], $migrationContent);
        
        $blocksForStructure = $contentMakeBlocks['dataInStructureList'];
        
        foreach ($blocksForStructure as &$item) {
            $item = "\t\t" . '$blocks[] = json_decode(\'' . $item . '\', true, 512, JSON_THROW_ON_ERROR);' . "\n";
        }
        unset($item);
        
        return (new MigrationData())
            ->setAddBLocksContent($addBLocksContent)
            ->setBlocksForStructure($blocksForStructure)
            ->setBlocksForCreate($contentMakeBlocks['blockForCreate'])
            ->setFieldsForAppend($contentMakeBlocks['fieldsForAppend']);
    }
    
    public function getContentMakeBlocks(): array
    {
        $blockContentList       = [];
        $removeBlockContentList = [];
        $blockContentUpList     = [];
        $dataInStructureList    = [];
        
        $fieldsForAppend = [];
        
        $blockForCreate = [];
        
        $this->blockInvited = [];
        
        foreach ($this->data as $datum) {
            
            if (array_key_exists('_template', $datum) && count($datum) === 1) {
                $dataInStructureList[] = json_encode($datum);
                continue;
            }
            
            $originalData = $datum;
            
            $template = $datum['_template'];
            
            $potentialName = $datum['__potentialName'] ?? '';
            unset($datum['__potentialName'], $originalData['__potentialName']);
            
            $blockClassName = $this->getBlockClassName($template);
            
            /** @noinspection PhpUndefinedMethodInspection */
            $hasOnlyTemplateMethod = $blockClassName::hasOnlyTemplateMethod();
            /** @noinspection PhpUndefinedMethodInspection */
            $hasListFromFactory = $blockClassName::hasList();
            
            $blockDetected = $this->blockDetector->detect($template);
            
            $entityApiCode = ucfirst($template);
            
            if ($blockDetected->getDetectEntitySuccess()) {
                $entityApiCode = $blockDetected->getEntityApiCode();
            }
            
            $sectionName = $this->getMainNameFieldName($datum, 'data', $potentialName, $template);
            $json        = json_encode($datum, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE);
            
            $blockTypeDetector = new BlockTypeDetector($datum);
            
            $isList = $blockTypeDetector->hasList();
            
            if ($blockDetected->isDetectTypeSuccess()) {
                $isList = $blockDetected->getIsList();
            }
            
            if ($hasListFromFactory !== null) {
                $isList = $hasListFromFactory;
            }
            
            /** @noinspection PhpUndefinedMethodInspection */
            $listField = $blockClassName::getListField();
            
            if (!$blockDetected->getBlockExists()) {
                $blockForCreate[] = [
                    'apiCode'   => $entityApiCode,
                    'hasList'   => $isList,
                    'listField' => $blockTypeDetector->getListField(),
                    'data'      => $originalData,
                ];
            }
            
            if ($isList) {
                if ($listField === null) {
                    $listField = $blockTypeDetector->getListField();
                    
                    if ($blockDetected->isDetectIterableField()) {
                        $listField = $blockDetected->getIterableField();
                    }
                }
                
                $forList        = ContentReplacer::getForList($datum[$listField], $listField, $template);
                $datum          = $forList['list'];
                $configureTypes = $forList['configureTypes'];
                
                $templateContentFileName = '/block_content_list.tpl';
            } else {
                $configureTypes          = null;
                $templateContentFileName = '/block_content_item.tpl';
            }
            
            if ($hasOnlyTemplateMethod) {
                $templateContentFileName = '/block_content_method.tpl';
            }
            
            $iterateVariable = $isList ? 'datum' : 'data';
            
            $templateContent = file_get_contents($this->templatePath . $templateContentFileName);
            
            if (array_key_exists($entityApiCode, $this->blockInvited)) {
                $this->blockInvited[$entityApiCode]++;
            } else {
                $this->blockInvited[$entityApiCode] = 1;
            }
            
            $number = $this->blockInvited[$entityApiCode];
            
            $worldNumber          = '';
            $worldNumberForMethod = '';
            if ($number > 1) {
                $worldNumber = $this->numberWorlder->convert($number);
                
                $worldNumberForMethod = preg_replace_callback('/\-\w/', static function ($matches) {
                    return str_replace('-', '', strtoupper($matches[0]));
                }, $worldNumber);
                
                $worldNumberForMethod = ucfirst($worldNumberForMethod);
            }
            
            /** @noinspection PhpUndefinedMethodInspection */
            $block = new $blockClassName(
                $blockClassName::hasOriginalBlockData() ? $originalData : $datum,
                $iterateVariable, $configureTypes);
            
            if ($block instanceof IContextOperator) {
                $block->setContext($this);
            }
            
            /**
             * @var $block IBlock
             */
            
            $generatedCode = $block->getGenerateCode();
            
            $preparing = $block->getPrepareGenerateCode();
            
            $removeBlockContentList[] = $block->getRemoveCode($this->route, $entityApiCode, $worldNumber);
            
            $afterMethodCode = $block->getGenerateAfterMethodCode();
            
            $methodName = ucfirst($template) . $worldNumberForMethod;
            
            $blockContentUpList[] = '$this->make' . $methodName . '();';
            $sectionCode          = '';
            
            if ($hasOnlyTemplateMethod) {
                $templateContent = str_replace([
                    '#METHOD_NAME#',
                    '#METHOD_BODY#',
                    '#AFTER_METHOD_CODE#',
                ], [
                    $methodName,
                    $generatedCode,
                    $afterMethodCode,
                ], $templateContent);
            } else {
                $sectionCode = $block::getSectionCode($this->route, $worldNumber);
                
                $templateContent = str_replace([
                    '#PROPERTY_LIST#',
                    '#JSON#',
                    '#SECTION_CODE#',
                    '#METHOD_NAME#',
                    '#ENTITY_API_CODE#',
                    '#NAME#',
                    '#SECTION_NAME#',
                    '#PREPARING#',
                    '#LIST#',
                    '#AFTER_METHOD_CODE#',
                ], [
                    $generatedCode,
                    $json,
                    $sectionCode,
                    $methodName,
                    $entityApiCode,
                    $this->getMainNameFieldName($datum, $iterateVariable, $potentialName, $template),
                    $sectionName,
                    $preparing,
                    $listField,
                    $afterMethodCode,
                ], $templateContent);
            }
            
            $dataInStructureList[] = $block->getDataInStructure(
                $originalData,
                $isList,
                $entityApiCode,
                $sectionCode,
                $listField
            );
            
            $blockContentList[] = $templateContent;
            //addProps
            if ($blockDetected->getBlockExists()) {
                foreach ($block->detectingFields($blockDetected, $datum, $entityApiCode) as $detectingFieldBlockApiCode => $detectingFieldBlock) {
                    if (!array_key_exists($entityApiCode, $fieldsForAppend)) {
                        $fieldsForAppend[$detectingFieldBlockApiCode] = [];
                    }
                    
                    foreach ($detectingFieldBlock as $key => $field) {
                        if (!array_key_exists($key, $fieldsForAppend[$detectingFieldBlockApiCode])) {
                            $fieldsForAppend[$detectingFieldBlockApiCode][$key] = $field;
                        }
                        
                        $fieldsForAppend[$detectingFieldBlockApiCode][$key] =
                            FieldParser::compareTypes($fieldsForAppend[$detectingFieldBlockApiCode][$key], $field);
                    }
                }
            }
        }
        
        return [
            'blockContentUpList'     => $blockContentUpList,
            'removeBlockContentList' => array_reverse($removeBlockContentList),
            'blockContentList'       => $blockContentList,
            'dataInStructureList'    => $dataInStructureList,
            'blockForCreate'         => $blockForCreate,
            'fieldsForAppend'        => $fieldsForAppend,
        ];
    }
    
    private function getBlockClassName(string $template): string
    {
        /**
         * @var $block IBlock
         */
        foreach (self::BLOCKS as $block) {
            if ($block::getName() === $template) {
                return $block;
            }
            
        }
        
        return BaseBlock::class;
    }
    
    private function getMainNameFieldName(array $data, string $variable, string $potentialName, string $blockName): string
    {
        try {
            $field = lcfirst((new FieldParser())->getMainNameFieldName($data, $blockName));
            
            return $data[$field] ? ('$' . $variable . '[\'' . $field . '\']') : "'$potentialName'";
        } catch (Exception $exception) {
            return $potentialName ? "'$potentialName'" : "'Для страницы \"$this->titlePage\"'";
        }
    }
    
    /**
     * @param  $self $this
     * @return $this
     */
    public function selfClone($self, ...$params): self
    {
        $selfInstance = new self(
            $params[0] ?? [],
            $self->templatePath,
            $self->route,
            $self->titlePage,
            $self->blockDetector);
        
        $selfInstance->blockInvited = $this->blockInvited;
        
        return $selfInstance;
    }
    
    /**
     * @return string
     */
    public function getTitlePage(): string
    {
        return $this->titlePage;
    }
}
