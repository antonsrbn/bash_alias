<?php

namespace Tools\Blocks\DTO;

class MigrationData
{
    /**
     * @var string
     */
    private $addBLocksContent;
    /**
     * @var string[]
     */
    private $blocksForStructure;
    /**
     * @var array
     */
    private $blocksForCreate;
    
    /**
     * @var array[]
     */
    private $fieldsForAppend;
    
    /**
     * @return string
     */
    public function getAddBLocksContent(): string
    {
        return $this->addBLocksContent;
    }
    
    /**
     * @param string $addBLocksContent
     * @return MigrationData
     */
    public function setAddBLocksContent(string $addBLocksContent): MigrationData
    {
        $this->addBLocksContent = $addBLocksContent;
        return $this;
    }
    
    /**
     * @return string[]
     */
    public function getBlocksForStructure(): array
    {
        return $this->blocksForStructure;
    }
    
    /**
     * @param string[] $blocksForStructure
     * @return MigrationData
     */
    public function setBlocksForStructure(array $blocksForStructure): MigrationData
    {
        $this->blocksForStructure = $blocksForStructure;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getBlocksForCreate(): array
    {
        return $this->blocksForCreate;
    }
    
    /**
     * @param array $blocksForCreate
     * @return MigrationData
     */
    public function setBlocksForCreate(array $blocksForCreate): MigrationData
    {
        $this->blocksForCreate = $blocksForCreate;
        return $this;
    }
    
    /**
     * @return array[]
     */
    public function getFieldsForAppend(): array
    {
        return $this->fieldsForAppend;
    }
    
    /**
     * @param array[] $fieldsForAppend
     * @return $this
     */
    public function setFieldsForAppend(array $fieldsForAppend): self
    {
        $this->fieldsForAppend = $fieldsForAppend;
        return $this;
    }
    
}
