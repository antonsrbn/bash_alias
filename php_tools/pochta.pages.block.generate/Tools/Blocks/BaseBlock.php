<?php

namespace Tools\Blocks;

use Symfony\Component\VarDumper\VarDumper;
use Tools\FieldParser;
use Tools\FieldsFactory\FileField;
use Tools\FieldsFactory\IField;
use Tools\NameTranslator;

class BaseBlock implements IBlock
{
    /**
     * @var array
     */
    protected $blockData;
    /**
     * @var IField[]
     */
    protected $fieldTypes;
    /**
     * @var string
     */
    protected $elementVariable;
    
    
    public function __construct(array $blockData, string $elementVariable, ?array $fieldTypes = null)
    {
        $this->blockData = $blockData;
        if ($fieldTypes) {
            $this->fieldTypes = $fieldTypes;
        } else {
            $this->fieldTypes = (new FieldParser())->getFieldTypes($this->blockData, $this->blockData['_template']);
        }
        
        $this->elementVariable = $elementVariable;
    }
    
    public static function getName(): string
    {
        return '';
    }
    
    public function getGenerateCode(): string
    {
        $types = $this->fieldTypes;
        
        $contentFields = [];
        foreach ($this->blockData as $key => $blockDatum) {
            if ($key === '_template') {
                continue;
            }
            
            if ($key === 'name') {
                continue;
            }
            
            $translator  = new NameTranslator($key);
            $makeContent = $types[$key]->makeForBlockMigration();
            
            $makeContent = $this->makeField($translator, $makeContent);
            
            $contentFields[] = $this->makeRow($translator, $makeContent);
        }
        
        return implode("\n", $contentFields);
    }
    
    protected function makeField(NameTranslator $translator, string $makeContent): string
    {
        return str_replace('#FIELD#', "\$$this->elementVariable['{$translator->getOriginName()}']", $makeContent);
    }
    
    
    protected function makeRow(NameTranslator $translator, string $makeContent): string
    {
        return "\t\t" . "'{$translator->getColumnName()}' => $makeContent,";
    }
    
    /**
     * @return string
     */
    public function getPrepareGenerateCode(): string
    {
        $types = $this->fieldTypes;
        
        $contentFields = [];
        foreach ($this->blockData as $key => $blockDatum) {
            if ($key === '_template') {
                continue;
            }
            $makeContent = $types[$key]->makePrepareDataBlockMigration();
            
            if ($makeContent) {
                $makeContent     = str_replace('#FIELD#', "\$$this->elementVariable['$key']", $makeContent);
                $contentFields[] = "\t\t" . $makeContent;
            }
        }
        
        return implode("\n", $contentFields);
    }
    
    public function getRemoveCode(string $route, string $apiCode, ?string $worldNumber): string
    {
        $sectionCode = self::getSectionCode($route, $worldNumber);
        
        return 'BlockMaker::get()->removeSectionByCodePath(\'' . $apiCode . '\', \'' . $sectionCode . '\');';
    }
    
    public static function getSectionCode(string $route, ?string $worldNumber): string
    {
        return strtolower(str_replace(['/', '_'], '-', $route)) . ($worldNumber ? ('-' . $worldNumber) : '');
    }
    
    public static function hasOnlyTemplateMethod(): bool
    {
        return false;
    }
    
    public static function hasOriginalBlockData(): bool
    {
        return false;
    }
    
    public function getGenerateAfterMethodCode(): string
    {
        return '';
    }
    
    public static function hasList(): ?bool
    {
        return null;
    }
    
    public static function getListField(): ?string
    {
        return null;
    }
    
    public function getDataInStructure(
        array $originalData,
        bool $hasList,
        string $apiCode,
        string $sectionCode,
        ?string $iterableField = null
    ): string {
        $template = $originalData['_template'];
        
        if ($hasList) {
            $originalData[$iterableField] = [];
        } else {
            $originalData = [];
        }
        
        $originalData['_template'] = $template;
        
        $originalData['section'] = '#GET_SECTION_CODE#';
        
        $this->replayJsonFileSave($originalData);
        
        $json = json_encode($originalData, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE);
        
        $json = str_replace(
            '"#GET_SECTION_CODE#"',
            '\' . BlockMaker::get()->getSectionId(\'' . $apiCode . '\', \'' . $sectionCode . '\') . \'',
            $json);
        
        return preg_replace_callback(
            '/#SAVE_FILE_CODE_START#(.+?)#SAVE_FILE_CODE_END#/', static function ($matches) {
            $path = str_replace('\/', '/', $matches[1]);
            
            return '\' . BlockMaker::get()->saveImageGetPath(\'' . $path . '\') . \'';
        },
            $json);
    }
    
    protected function replayJsonFileSave(array &$data): void
    {
        foreach ($data as $key => &$item) {
            if (is_array($item)) {
                
                self::replayJsonFileSave($item);
            } elseif (FileField::hasCondition($key, $item)) {
                $item = '#SAVE_FILE_CODE_START#' . $item . '#SAVE_FILE_CODE_END#';
            }
        }
    }
    
    public function detectingFields(IBlockContentDetector $blockContentDetector, array $blocksData, string $apiCode): array
    {
        $types   = $this->fieldTypes;
        $logList = [];
        
        foreach ($this->blockData as $key => $blockDatum) {
            if ($key === '_template') {
                continue;
            }
            
            if ($key === 'name') {
                continue;
            }
            if (!$this->hasFieldExist($blockContentDetector, $key)) {
                $logList[$key] = $types[$key];
            }
        }
        
        return [$apiCode => $logList];
    }
    
    
    public function hasFieldExist(IBlockContentDetector $blockContentDetector, string $key): bool
    {
        if ($contentFile = $blockContentDetector->getDtoContent()) {
            $content = $contentFile->getContent();
            
            $matches = [];
            preg_match_all('/fromArray\((.+?)\):(.+?)return\s+(.+?);/s', $content, $matches);
            
            $varMatches = [];
            preg_match_all('/array\s+\$(.+)/s', $matches[1][0] ?? '', $varMatches);
            $varData = $varMatches[1][0] ?? '';
            $content = $matches[3][0] ?? '';
            
            $matches = [];
            return preg_match_all('/\\$' . $varData . '\[\'' . $key . '\']/s', $content, $matches);
        }
        
        return false;
    }
    
    public function getDetectField(IBlockContentDetector $blockContentDetector, string $key): ?IField
    {
        // TODO: Implement getDetectField() method.
    }
    
    public function appendField(IBlockContentDetector $blockContentDetector, IField $field): bool
    {
        // TODO: Implement appendField() method.
    }
}
