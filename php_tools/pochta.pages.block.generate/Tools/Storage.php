<?php

namespace Tools;

use Tools\Detectors\DTO\TinaBlock;

/**
 * Class Storage.
 *
 * @package Tools
 */
class Storage
{
    /**
     * @var TinaBlock[]
     */
    private static $tinaBlocks;
    
    private static $tick;
    
    /**
     * @return TinaBlock[]
     */
    public static function getTinaBlocks(): array
    {
        return self::$tinaBlocks ?? [];
    }
    
    /**
     * @param TinaBlock[] $tinaBlocks
     */
    public static function setTinaBlocks(array $tinaBlocks): void
    {
        self::$tinaBlocks = $tinaBlocks;
    }
    
    public static function tick(): int
    {
        if (self::$tick === null) {
            self::$tick = 0;
        }
        
        self::$tick+=2;
        return self::$tick;
    }
}
