<?php

namespace Tools;

/**
 * Class NameTranslator.
 *
 * @package Tools
 */
class NameTranslator
{
    /**
     * @var string
     */
    private $name;
    
    public function __construct(string $name)
    {
        $this->name = $name;
    }
    
    public function getVariableName(): string
    {
        return $this->name;
    }
    
    public function getColumnName(): string
    {
        $replace = preg_replace('/([A-Z])/u', '_$0', $this->name);
        $replace = preg_replace('/^_/u', '', $replace);
        
        return strtoupper($replace);
    }
    
    public function getForMethodName(): string
    {
        return ucfirst($this->name);
    }
    
    public function getOriginName(): string
    {
        return $this->name;
    }
    
    public function getRussianName(?string $alternativeRussianName = null): string
    {
        if ($alternativeRussianName) {
            return $alternativeRussianName;
        }
      
        $names = [
            'ACCENT_TEXT'                 => 'Акцентированный текст',
            'ADVANTAGES'                  => 'Преимущества',
            'ANIMATION_TYPE'              => 'Цвет анимации',
            'ARCHIVE'                     => 'Является ли карта архивной',
            'ASSIGNMENT'                  => 'Назначение страницы',
            'AVAILABLE_IN_REGION'         => 'Доступность в регионе',
            'AWARDS_LIST'                 => 'Список наград',
            'BACKGROUND'                  => 'background',
            'BACKGROUND_IMAGE'            => 'Изображение бэкграунд',
            'BANNER_ANIMATION_IMAGE'      => 'Анимированная изображение',
            'BANNER_BACKGROUND'           => 'Фон баннера',
            'BANNER_BUTTONS'              => 'Кнопки баннера',
            'BANNER_DESCRIPTION'          => 'Описание баннера',
            'BANNER_DOT_GTAG'             => 'BANNER_DOT_GTAG',
            'BANNER_IMAGE'                => 'Изображение баннера',
            'BANNER_SUBTITLE'             => 'Подзаголовок баннера',
            'BANNER_TITLE'                => 'Заголовок баннера',
            'BANNER_WITH_ANIMATION_IMAGE' => 'Включить/выключить анимированная изображение',
            'BG'                          => 'BG',
            'BG_COLOR'                    => 'Цвет фона ',
            'BIG'                         => 'Большой элемент',
            'BLOCKS'                      => 'Блоки (скрытое поле)',
            'BONUSCARD'                   => 'Бонусная карта',
            'BOTTOM_TEXT'                 => 'Текст снизу',
            'BREADCRUMBS'                 => 'Хлебные крошки',
            'BTN'                         => 'Кнопка',
            'BTN_HREF'                    => 'Ссылка кнопки',
            'BTN_LABEL'                   => 'Текст кнопки',
            'BTN_LINK'                    => 'Ссылка кнопки',
            'BTN_LINK_NAME'               => 'Текст на кнопке',
            'BUTTON'                      => 'Кнопка',
            'BUTTON_AR_LINK'              => 'Кнопка для АРХИВ "Подробнее" Ссылка',
            'BUTTON_AR_TEXT'              => 'Кнопка для АРХИВ "Подробнее" Текст',
            'BUTTON_BG'                   => 'Кнопка BG',
            'BUTTON_BG_LINK'              => 'Кнопка "Заказать Карту" Ссылка',
            'BUTTON_BG_TEXT'              => 'Кнопка "Заказать Карту" Текст',
            'BUTTON_DETAIL'               => 'Кнопки',
            'BUTTON_DT_LINK'              => 'Кнопка "Подробнее" Ссылка',
            'BUTTON_DT_TEXT'              => 'Кнопка "Подробнее" Текст',
            'BUTTONS'                     => 'Кнопки',
            'CARD_LAYER_BG'               => 'Изображение карты бэкграунд',
            'CARD_LAYER_INFO'             => 'Изображение карты инфо',
            'CATEGORIES'                  => 'Категории',
            'COLOR'                       => 'Цвет',
            'CONDITIONS'                  => 'Условия',
            'CONDITIONS_IMAGE'            => 'Условия - изображение',
            'CONDITIONS_SUBHEAD'          => 'Условия - подзаголовок',
            'CONDITIONS_TEXT'             => 'Условия - текст',
            'CUSTOM_PROPERTY_BLOCK'       => 'Блоки',
            'DATA'                        => 'Заголовок',
            'DATE'                        => 'Год',
            'DATE_DOC_END'                => 'Актуальность документа - конец',
            'DATE_DOC_START'              => 'Актуальность документа - старт',
            'DEMONSTRATION_BLOCK'         => 'Блок отображения',
            'DESC'                        => 'Описание',
            'DESCRIPTION'                 => 'Описание',
            'DESCRIPTION_MOBILE'          => 'Описание баннера на мобильной версии',
            'DOCUMENT'                    => 'Документ',
            'DOCUMENT_NAME'               => 'Название документы',
            'EMAIL'                       => 'Email',
            'FIO'                         => 'ФИО',
            'FOOTER'                      => 'Footer',
            'GA_PARAMS'                   => 'Параметры google analytics',
            'GTAG'                        => 'GTAG',
            'HEADER'                      => 'Header',
            'HEADING'                     => 'Заголовок',
            'HIDE_TITLE'                  => 'Скрыть заголовок',
            'HTML'                        => 'HTML',
            'IMAGE'                       => 'Изображение',
            'IMAGE_DARK_THEME'            => 'Изображение (Темная тема)',
            'IMAGE_MIN_HIGH'              => 'Изображение',
            'IMAGE_MIN_LOW'               => 'Изображение',
            'IMAGE_POSITION'              => 'IMAGE_POSITION',
            'IMAGEALT'                    => 'Подпись под картинкой',
            'IMG'                         => 'Изображение',
            'INFO'                        => 'Описание тарифа',
            'IS_BIG_ICON'                 => 'Большая иконка',
            'IS_CONDITIONS_ACTIVE'        => 'Активность условия',
            'IS_LINK'                     => 'Есть ссылка?',
            'LINE'                        => 'line',
            'LINK'                        => 'Ссылка',
            'LINK_HREF'                   => 'Кнопка. Ссылка',
            'LINK_NAME'                   => 'Текст ссылки',
            'LINK_TEXT'                   => 'Текст ссылки',
            'LIST'                        => 'Список сервисов',
            'LOGO'                        => 'Иконка',
            'MAIN'                        => 'Подсветка элемента карты',
            'MARGIN'                      => 'Отступ',
            'MARK_CONTENT'                => 'Кнопка с подсказкой. Контент',
            'MARK_NAME'                   => 'Кнопка с подсказкой. Название',
            'META'                        => 'Meta',
            'MOBILE_DISPLAY'              => 'Мобильное отображение?',
            'NEWS_DATE'                   => 'Дата новости',
            'NUMBER'                      => 'Номер',
            'PAGE_PARAMS'                 => 'Page params',
            'PARENT_PAGE'                 => 'Наследовать основное содержимое от страницы : ',
            'PATH'                        => 'Путь',
            'PAYMENT'                     => 'Список сервисов оплат',
            'PERMANENT_DOCUMENT'          => 'Постоянная ссылка на документ',
            'PHONE'                       => 'Телефон',
            'POSITION_IMG'                => 'POSITION_IMG',
            'PRODUCT_PARAM_VALUE'         => 'Первое поле seo текста',
            'PRODUCT_TYPE'                => 'Вид продукта',
            'PROMO'                       => 'Промо',
            'QR'                          => 'QR',
            'QR_CODE'                     => 'QR-код',
            'SCHEMA'                      => 'Кастомные параметры для schema.org',
            'SEO_TEXT_FIRST'              => 'Первое поле seo текста',
            'SEO_TEXT_SECOND'             => 'Второе поле seo текста',
            'SEO_TEXT_THIRD'              => 'Третье поле seo текста',
            'SIDE_IMG'                    => 'SIDE_IMG',
            'SIDEBAR'                     => 'Сайдбар',
            'SIZE'                        => 'Размер блока',
            'SOCIAL_LIST'                 => 'Социальные сети',
            'STARS'                       => 'Оценка',
            'STORE'                       => 'Магазин',
            'SUBHEAD'                     => 'Подаголовок',
            'SUBHEADING'                  => 'Подзаголовок',
            'SUBTITLE'                    => 'Подзаголовок',
            'SUM'                         => 'Сумма',
            'TAG_LIST'                    => 'Список тегов',
            'TEMPLATE'                    => 'Шаблон',
            'TEST'                        => 'Тестовое свойство',
            'TEXT'                        => 'Текст',
            'TEXT_BNT'                    => 'Текст кнопки',
            'TEXT_LINK'                   => 'Текст ссылки',
            'TITLE'                       => 'Заголовок',
            'TITLE_MOBILE'                => 'Заголовок для мобильной версии',
            'TOOLTIP_TEXT'                => 'Всплывающая подсказка',
            'TOP_TEXT'                    => 'Текст сверху',
            'TYPE'                        => 'TYPE',
            'VIDEO'                       => 'Видео',
            'WITH_ANIMATE'                => 'Подключить анимацию?',
            'WITH_DECOR'                  => 'Декор',
            'YEAR'                        => 'Годы',
            'PHOTO'                       => 'Фото',
            'POSITION'                    => 'Позиция',
            'QUOTE'                       => 'Цитата',
            'SUCCESS_TITLE'               => 'Текст успешного результата',
        ];
        return $names[$this->getColumnName()] ?? $this->name;
    }
    
    public function getStyleCodeName(): string
    {
        $replace = preg_replace('/([A-Z])/u', '-$0', $this->name);
        $replace = preg_replace('/^-/u', '', $replace);
        
        return strtolower($replace);
    }
    
    public function getFileCodeName(): string
    {
        $replace = preg_replace('/([A-Z])/u', '_$0', $this->name);
        $replace = preg_replace('/[\/-]/u', '_', $replace);
        $replace = preg_replace('/^_/u', '', $replace);
        
        return strtolower($replace);
    }
}
