<?php

namespace Tools\FieldsFactory;

/**
 * Class TextField.
 *
 * @package Tools\FieldsFactory
 */
class TextField extends BaseField
{
    public static function hasCondition(string $key, $value): bool
    {
        $value = trim($value);
        if (in_array($key, ['desc', 'description'])) {
            return true;
        }
        
        if (mb_strlen($value) > 100) {
            return true;
        }
        
        if (preg_replace([
                "/\n/",
                '/<(.+?)>/',
                '/<(.+?)\/>/',
                '/<(.+?)>(.+?)<\/(.+?)>/',
            ], '', $value) !== $value) {
            return true;
        }
        
        return false;
    }
    
    public function makeForRepositoryInitObject(): ?string
    {
        if (in_array($this->key, self::EXCLUDING)) {
            return "\t\t" . '$object->set' . $this->translator->getForMethodName() . '($item->get' . $this->translator->getForMethodName() . '());';
        }
        
        return "\t\t" . '$object->set' . $this->translator->getForMethodName() . '(self::encodeStringToHtmlTextProperty($item->get' . $this->translator->getForMethodName() . '()));';
    }
    
    public function makeForDtoFromObject(): ?string
    {
        if (in_array($this->key, self::EXCLUDING)) {
            return "\t\t" . '$'
                . $this->translator->getOriginName()
                . ' = $object->get'
                . $this->translator->getForMethodName()
                . '();';
        }
        
        return "\t\t" . ' $' . $this->translator->getVariableName() . ' = self::decodeHtmlTextPropertyToString(
            $object->get' . $this->translator->getForMethodName() . '()
                ? $object->get' . $this->translator->getForMethodName() . '()->getValue()
                : \'\'
        );' . "\n";
    }
    
    public function makeMigrationProperty(): ?string
    {
        if (in_array($this->key, self::EXCLUDING)) {
            return null;
        }
        
        return "\t\t\t" . '(new CIBlockProperty())->Add([
                \'NAME\'          => \'' . $this->translator->getRussianName($this->alternativeRussianName) . '\',
                \'CODE\'          => \'' . $this->translator->getColumnName() . '\',
                \'PROPERTY_TYPE\' => \'S\',
                \'USER_TYPE\'     => \'HTML\',
                \'IBLOCK_ID\'     => $id,
                \'IS_REQUIRED\'   => \'N\'
            ]);' . "\n";
    }
    
    public function makeForBlockMigration(): string
    {
        return "BlockMaker::get()->htmlText(#FIELD#)";
    }
}
