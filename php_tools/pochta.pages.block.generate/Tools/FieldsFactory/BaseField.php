<?php

namespace Tools\FieldsFactory;

use Tools\NameTranslator;

/**
 * Class BaseField.
 *
 * @package Tools\FieldsFactory
 */
class BaseField implements IField
{
    protected const EXCLUDING = ['name', 'previewText', 'detailText'];
    /**
     * @var string
     */
    protected $key;
    protected $value;
    /**
     * @var NameTranslator
     */
    protected $translator;
    /**
     * @var string|null
     */
    protected $alternativeRussianName;
    
    public function __construct(string $key, $value, ?string $alternativeRussianName = null)
    {
        $this->translator             = new NameTranslator($key);
        $this->key                    = $key;
        $this->value                  = $value;
        $this->alternativeRussianName = $alternativeRussianName;
    }
    
    public static function hasCondition(string $key, $value): bool
    {
        return true;
    }
    
    public function makeForRepositoryInitObject(): ?string
    {
        if (in_array($this->key, self::EXCLUDING)) {
            return "\t\t" . '$object->set' . $this->translator->getForMethodName() . '($item->get' . $this->translator->getForMethodName() . '());';
        }
        
        return "\t\t" . '$object->set' . $this->translator->getForMethodName() . '($item->get' . $this->translator->getForMethodName() . '() ?: \'\');';
    }
    
    public function makeForRepositoryAfterCreateObject(): ?string
    {
        //"\t\t\t\t"
        return null;
    }
    
    public function makeForDtoProperty(): ?string
    {
        return "\t" . 'private string $' . $this->translator->getVariableName() . ';';
    }
    
    public function makeForDtoFromArray(): ?string
    {
        return "\t\t\t" . '->set' . $this->translator->getForMethodName() . '((string)$data[\'' . $this->translator->getOriginName() . '\'])';
    }
    
    public function makeForDtoFromObject(): ?string
    {
        if (in_array($this->key, self::EXCLUDING)) {
            return "\t\t" . '$'
                . $this->translator->getOriginName()
                . ' = $object->get'
                . $this->translator->getForMethodName()
                . '();';
        }
        
        return "\t\t" . '$'
            . $this->translator->getOriginName()
            . ' = $object->get'
            . $this->translator->getForMethodName()
            . '() ? $object->get'
            . $this->translator->getForMethodName()
            . '()->getValue() : \'\';';
    }
    
    public function makeForDtoFromObjectDto(): ?string
    {
        return "\t\t\t" . '->set' . $this->translator->getForMethodName() . '($' . $this->translator->getVariableName() . ')';
        
    }
    
    public function makeForDtoToArray(): ?string
    {
        return "\t\t\t" . '\'' . $this->translator->getOriginName() . '\' => $this->get' . $this->translator->getForMethodName() . '(),';
    }
    
    public function makeForDtoGetter(): ?string
    {
        return '     /**
     * @return string
     */
    public function get' . $this->translator->getForMethodName() . '(): string
    {
        return $this->' . $this->translator->getVariableName() . ';
    }' . "\n";
    }
    
    public function makeForDtoSetter(): ?string
    {
        return '     /**
     * @param string $' . $this->translator->getVariableName() . '
     * @return $this
     */
    public function set' . $this->translator->getForMethodName() . '(string $' . $this->translator->getVariableName() . '): self
    {
        $this->' . $this->translator->getVariableName() . ' = $' . $this->translator->getVariableName() . ';
        return $this;
    }' . "\n";
    }
    
    public function makeForHandlerExcludingFields(): ?string
    {
        return "\t\t\t" . ' \'' . $this->translator->getOriginName() . '\',';
    }
    
    public function makeForTableQuery(): ?string
    {
        if (in_array($this->key, self::EXCLUDING)) {
            return null;
        }
        
        return "\t\t\t" . '->addSelect(\'' . $this->translator->getColumnName() . '\')';
    }
    
    public function makeMigrationProperty(): ?string
    {
        if (in_array($this->key, self::EXCLUDING)) {
            return null;
        }
        
        return "\t\t\t" . '(new CIBlockProperty())->Add([
                \'NAME\'          => \'' . $this->translator->getRussianName($this->alternativeRussianName) . '\',
                \'CODE\'          => \'' . $this->translator->getColumnName() . '\',
                \'PROPERTY_TYPE\' => \'S\',
                \'IBLOCK_ID\'     => $id,
                \'IS_REQUIRED\'   => \'N\'
            ]);' . "\n";
    }
    
    public function makeForRepositoryAfterCode(): ?string
    {
        return null;
    }
    
    public function makeForBlockMigration(): string
    {
        return '#FIELD#';
    }
    
    public function makePrepareDataBlockMigration(): ?string
    {
        return null;
    }
    
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * @param string $className
     * @return IField
     */
    public function makeOtherTypeField(string $className): IField
    {
        /**
         * @var $className IField
         */
        return new $className($this->key, $this->value, $this->alternativeRussianName);
    }
}
