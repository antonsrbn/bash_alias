<?php

namespace Tools\FieldsFactory;

/**
 * Class EnumField.
 *
 * @package Tools\FieldsFactory
 */
class EnumField extends BaseField
{
    public static function hasCondition(string $key, $value): bool
    {
        return false;
    }
    
    
    public function makeForBlockMigration(): string
    {
        return "strtoupper(#FIELD#)";
    }
}
