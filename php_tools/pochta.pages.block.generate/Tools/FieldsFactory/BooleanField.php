<?php

namespace Tools\FieldsFactory;

use Pochtabank\Engine\Orm\EnumTable;

/**
 * Class BooleanField.
 *
 * @package Tools\FieldsFactory
 */
class BooleanField extends BaseField
{
    public static function hasCondition(string $key, $value): bool
    {
        if (is_bool($value)) {
            return true;
        }
        
        return false;
    }
    
    public function makeForRepositoryInitObject(): ?string
    {
        return "\t\t".'if ($item->get' . $this->translator->getForMethodName() . '()) {
            $' . $this->translator->getVariableName() . ' = $this->get' . $this->translator->getForMethodName() . 'Enum();
            if ($' . $this->translator->getVariableName() . ') {
                $object->set' . $this->translator->getForMethodName() . '($' . $this->translator->getVariableName() . '->getId());
            }
        }'."\n";
    }
    
    public function makeForRepositoryAfterCreateObject(): ?string
    {
        return "\t\t\t\t".'\'' . $this->translator->getColumnName() . '.ITEM\',';
    }
    
    public function makeForDtoProperty(): ?string
    {
        return "\t".'private bool $' . $this->translator->getVariableName() . ';';
    }
    
    public function makeForDtoFromArray(): ?string
    {
        return  "\t\t\t".'->set' . $this->translator->getForMethodName() . '((bool)$data[\'' . $this->translator->getOriginName() . '\'])';
    }
    
    public function makeForDtoFromObject(): ?string
    {
        return "\t\t".'$' . $this->translator->getVariableName() . ' = $object->get' . $this->translator->getForMethodName() . '() && $object->get' . $this->translator->getForMethodName() . '()->getItem()->getXmlId() === \'YES\';';
    }
    
    public function makeForDtoGetter(): ?string
    {
        return '     /**
     * @return bool
     */
    public function get' . $this->translator->getForMethodName() . '(): bool
    {
        return $this->' . $this->translator->getVariableName() . ';
    }'."\n";
    }
    
    public function makeForDtoSetter(): ?string
    {
        return '     /**
     * @param bool $' . $this->translator->getVariableName() . '
     * @return $this
     */
    public function set' . $this->translator->getForMethodName() . '(bool $' . $this->translator->getVariableName() . '): self
    {
        $this->' . $this->translator->getVariableName() . ' = $' . $this->translator->getVariableName() . ';
        return $this;
    }'."\n";
    }
    
    public function makeForRepositoryAfterCode(): ?string
    {
        return '    private function get' . $this->translator->getForMethodName() . 'Enum(): ?EO_Enum
    {
        return EnumTable::getByApiCode(
            self::getIblock()->getApiCode(),
            \'' . $this->translator->getColumnName() . '\',
            \'YES\'
        );
    }'."\n";
    }
    public function makeForTableQuery(): ?string
    {
        return "\t\t\t".'->addSelect(\'' . $this->translator->getColumnName() . '\')' . "\n".
            "\t\t\t".'->addSelect(\'' . $this->translator->getColumnName() . '.ITEM\')';
    }
    public function makeMigrationProperty(): ?string
    {
        return "\t\t\t".'(new CIBlockProperty())->Add([
                \'NAME\'          => \''.$this->translator->getRussianName($this->alternativeRussianName).'\',
                \'CODE\'          => \'' . $this->translator->getColumnName() . '\',
                \'PROPERTY_TYPE\' => \'L\',
                \'LIST_TYPE\'     => \'C\',
                \'VALUES\'        => [
                    [
                        \'XML_ID\' => \'YES\',
                        \'VALUE\'  => \'Да\',
                    ],
                ],
                \'IBLOCK_ID\'     => $id,
                \'IS_REQUIRED\'   => \'N\'
            ]);'."\n";
    }
    
    public function makeForBlockMigration(): string
    {
        return "(#FIELD# ? 'YES' : null)";
    }
}
