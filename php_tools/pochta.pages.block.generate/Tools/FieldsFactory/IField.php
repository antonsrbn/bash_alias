<?php

namespace Tools\FieldsFactory;

/**
 * Interface IField.
 */
interface IField
{
    public function __construct(string $key, $value, ?string $alternativeRussianName = null);
    
    public function getValue();
    
    public static function hasCondition(string $key, $value): bool;
    
    public function makeForRepositoryInitObject(): ?string;
    
    public function makeForRepositoryAfterCreateObject(): ?string;
    
    public function makeForRepositoryAfterCode(): ?string;
    
    public function makeForDtoProperty(): ?string;
    
    public function makeForDtoFromArray(): ?string;
    
    public function makeForDtoFromObject(): ?string;
    
    public function makeForDtoFromObjectDto(): ?string;
    
    public function makeForDtoToArray(): ?string;
    
    public function makeForDtoGetter(): ?string;
    
    public function makeForDtoSetter(): ?string;
    
    public function makeForHandlerExcludingFields(): ?string;
    
    public function makeForTableQuery(): ?string;
    
    public function makeMigrationProperty(): ?string;
    
    public function makeForBlockMigration(): string;
    
    public function makePrepareDataBlockMigration(): ?string;
    
    public function makeOtherTypeField(string $className): IField;
}
