<?php

namespace Tools\FieldsFactory;

/**
 * Class FileField.
 *
 * @package Tools\FieldsFactory
 */
class FileField extends BaseField
{
    public static function hasCondition(string $key, $value): bool
    {
        if (str_replace(['https:', 'http:'], '', $value) !== $value) {
            return false;
        }
        
        if (in_array($key, ['img', 'image'])) {
            return true;
        }
        if (preg_match('/(.?)+\/(.?)+\.([\w])+$/ui', $value)) {
            return true;
        }
        return false;
    }
    
    public function makeForRepositoryInitObject(): ?string
    {
        return "\t\t" . '$' . $this->translator->getVariableName() . 'Id   = File::getIdFromUrl($item->get' . $this->translator->getForMethodName() . '());
        $' . $this->translator->getVariableName() . 'File = FileTable::getObjectById($' . $this->translator->getVariableName() . 'Id);
        if ($' . $this->translator->getVariableName() . 'File) {
            CFile::AddDuplicate($' . $this->translator->getVariableName() . 'File->getId());
            $object->set' . $this->translator->getForMethodName() . '($' . $this->translator->getVariableName() . 'File->getId());
        }' . "\n";
    }
    
    public function makeForRepositoryAfterCreateObject(): ?string
    {
        return "\t\t\t\t" . '\'' . $this->translator->getColumnName() . '.FILE\',';
    }
    
    public function makeForDtoFromObject(): ?string
    {
        return "\t\t"
            . ' $'
            . $this->translator->getVariableName()
            . 'File = $object->get'
            . $this->translator->getForMethodName()
            . '() ? $object->get'
            . $this->translator->getForMethodName()
            . '()->getFile() : null;
        $'
            . $this->translator->getVariableName()
            . '     = isset($'
            . $this->translator->getVariableName()
            . 'File) ? File::getSrc($'
            . $this->translator->getVariableName()
            . 'File) ?? \'\' : \'\';'
            . "\n";
    }
    
    public function makeForTableQuery(): ?string
    {
        return "\t\t\t" . '->addSelect(\'' . $this->translator->getColumnName() . '\')' . "\n" .
            "\t\t\t" . '->addSelect(\'' . $this->translator->getColumnName() . '.FILE\')';
    }
    
    public function makeMigrationProperty(): ?string
    {
        return "\t\t\t" . '(new CIBlockProperty())->Add([
                \'NAME\'          => \'' . $this->translator->getRussianName($this->alternativeRussianName) . '\',
                \'CODE\'          => \'' . $this->translator->getColumnName() . '\',
                \'PROPERTY_TYPE\' => \'F\',
                \'IBLOCK_ID\'     => $id,
                \'IS_REQUIRED\'   => \'Y\'
            ]);' . "\n";
    }
    
    
    public function makeForBlockMigration(): string
    {
        return "BlockMaker::get()->saveImage(#FIELD#)";
    }
}

