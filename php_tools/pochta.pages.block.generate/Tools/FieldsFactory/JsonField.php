<?php

namespace Tools\FieldsFactory;

use Symfony\Component\VarDumper\VarDumper;

/**
 * Class JsonFiled.
 *
 * @package Tools\FieldsFactory
 */
class JsonField extends BaseField
{
    public static function hasCondition(string $key, $value): bool
    {
        if (is_array($value)) {
            return true;
        }
        
        return false;
    }
    
    
    public function makeForRepositoryInitObject(): ?string
    {
        return "\t\t" . '$object->set' . $this->translator->getForMethodName() . '(self::encodeArrayToHtmlTextProperty($item->get' . $this->translator->getForMethodName() . '()));';
    }
    
    public function makeForDtoProperty(): ?string
    {
        return "\t" . 'private array $' . $this->translator->getVariableName() . ';';
    }
    
    public function makeForDtoFromArray(): ?string
    {
        return "\t\t\t" . '->set' . $this->translator->getForMethodName() . '((array)$data[\'' . $this->translator->getOriginName() . '\'])';
    }
    
    public function makeForDtoFromObject(): ?string
    {
        return "\t\t" . ' $' . $this->translator->getVariableName() . ' = $object->get' . $this->translator->getForMethodName() . '()
                ? self::decodeHtmlTextPropertyToArray(
             $object->get' . $this->translator->getForMethodName() . '()->getValue())
                : [];' . "\n";
    }
    
    public function makeForDtoGetter(): ?string
    {
        return '     /**
     * @return array
     */
    public function get' . $this->translator->getForMethodName() . '(): array
    {
        return $this->' . $this->translator->getVariableName() . ';
    }' . "\n";
    }
    
    public function makeForDtoSetter(): ?string
    {
        return '     /**
     * @param array $' . $this->translator->getVariableName() . '
     * @return $this
     */
    public function set' . $this->translator->getForMethodName() . '(array $' . $this->translator->getVariableName() . '): self
    {
        $this->' . $this->translator->getVariableName() . ' = $' . $this->translator->getVariableName() . ';
        return $this;
    }' . "\n";
    }
    
    public function makeMigrationProperty(): ?string
    {
        return "\t\t\t" . '(new CIBlockProperty())->Add([
                \'NAME\'          => \'' . $this->translator->getRussianName($this->alternativeRussianName) . '\',
                \'CODE\'          => \'' . $this->translator->getColumnName() . '\',
                \'PROPERTY_TYPE\' => \'S\',
                \'USER_TYPE\'     => \'HTML\',
                \'IBLOCK_ID\'     => $id,
                \'IS_REQUIRED\'   => \'N\'
            ]);' . "\n";
    }
    
    public function makeForBlockMigration(): string
    {
        return "#FIELD# ? BlockMaker::get()->arrayToJsonToHtmlText(#FIELD#) : null";
    }
    
    public function makePrepareDataBlockMigration(): ?string
    {
        $value    = $this->value;
        $hasFiles = false;
        
        $replacingList = [];
        
        $this->tree($value, $hasFiles, $replacingList, []);
        
        if ($hasFiles) {
            return implode("\n\t\t", $replacingList);
        }
        return null;
    }
    
    private function tree(array &$json, bool &$hasFiles, array &$replacingList, array $keys): void
    {
        foreach ($json as $key => &$item) {
            if (is_array($item)) {
                $newKeys   = $keys;
                $newKeys[] = $key;
                
                $this->tree($item, $hasFiles, $replacingList, $newKeys);
            } elseif (FileField::hasCondition($key, $item)) {
                $hasFiles = true;
                $keyChain = [];
                $keys[]   = $key;
                
                foreach ($keys as $levelKey) {
                    if (is_string($levelKey)) {
                        $keyChain[] = "'" . $levelKey . "'";
                    } else {
                        $keyChain[] = $levelKey;
                    }
                    
                }
                
                $chainCode       = '[' . implode('][', $keyChain) . ']';
                $replacingList[] = "if(#FIELD#$chainCode) {\n #FIELD#" . $chainCode . ' = BlockMaker::get()->saveImageGetPath(#FIELD#' . $chainCode . ");\n}";
            }
            
        }
    }
}
