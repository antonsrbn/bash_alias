<?php

namespace Tools;

use Symfony\Component\VarDumper\VarDumper;
use Tools\FieldsFactory\FileField;

class FindFileCloner
{
    /**
     * @var array
     */
    private $data;
    /**
     * @var string
     */
    private $sourcePath;
    /**
     * @var string
     */
    private $frontPath;
    /**
     * @var string
     */
    private $detectPath;
    
    public function __construct(
        array $data,
        string $sourcePath,
        string $detectPath,
        string $frontPath
    ) {
        $this->data       = $data;
        $this->sourcePath = $sourcePath;
        $this->detectPath = $detectPath;
        $this->frontPath  = $frontPath . '/public';
    }
    
    public function copyFiles(): array
    {
        $result = [];
        
        $value         = $this->data;
        $replacingList = [];
        
        $this->tree($value, $replacingList);
        
        if ($replacingList) {
            foreach ($replacingList as $item) {
                $from     = $this->frontPath . '/' . $item;
                $from     = str_replace('//', '/', $from);
                $to       = $this->sourcePath . '/' . $item;
                $to       = str_replace('//', '/', $to);
                $toDetect = $this->detectPath . '/' . $item;
                $toDetect = str_replace('//', '/', $toDetect);
                
                if (file_exists($from)) {
                    if (file_exists($to) || file_exists($toDetect)) {
                        $result['warning'][] = 'Файл уже существует ' . $toDetect;
                    } else {
                        if (!file_exists($toDetect)) {
                            
                            if (Helper::smartCopyFile($from, $to)) {
                                $result['info'][] = 'Файл скопирован в ' . $to;
                            } else {
                                $result['error'][] = 'Файл не был скопирован из ' . $from . ' в ' . $to;
                            }
                        }
                    }
                    
                } else {
                    $result['error'][] = 'Файл не существует ' . $from;
                }
                /*
                 VarDumper::dump([
                     'from'       => $from,
                     'to'         => $to,
                     'fromExists' => file_exists($from),
                     'toExists'   => file_exists($toDetect),
                 ]);*/
            }
            
        }
        
        return $result;
    }
    
    
    private function tree(array &$json, array &$replacingList): void
    {
        foreach ($json as $key => &$item) {
            if (is_array($item)) {
                
                $this->tree($item, $replacingList);
            } elseif (FileField::hasCondition($key, $item)) {
                $replacingList[] = $item;
            }
        }
    }
}
