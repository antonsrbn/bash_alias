<?php

namespace Tools;

use DateTime;
use DateTimeZone;

class CodeBlockLogicGenerator
{
    /**
     * @var string
     */
    private $templatePath;
    /**
     * @var string
     */
    private $generatePath;
    /**
     * @var IFactoryDetector
     */
    private $factoryDetector;
    
    
    public function __construct(
        string $templatePath,
        string $generatePath,
        IFactoryDetector $factoryDetector
    ) {
        
        $this->templatePath    = $templatePath . '/';
        $this->generatePath    = $generatePath . '/';
        $this->factoryDetector = $factoryDetector;
    }
    
    public function generate(array $blocksInfo): array
    {
        $newBlocksData = [];
        
        foreach ($blocksInfo as $item) {
            $apiCode   = $item['apiCode'];
            $hasList   = $item['hasList'];
            $listField = $item['listField'];
            $data      = $item['data'];
            
            if (array_key_exists($apiCode, $newBlocksData)) {
                if (!$newBlocksData[$apiCode]['hasList']) {
                    $newBlocksData[$apiCode]['hasList'] = $hasList;
                }
                
                if ($hasList) {
                    $newBlocksData[$apiCode]['data'] = Helper::array_merge_recursive_distinct($newBlocksData[$apiCode]['data'], $data[$listField]);
                } else {
                    $newBlocksData[$apiCode]['data'] = Helper::array_merge_recursive_distinct($newBlocksData[$apiCode]['data'], $data);
                }
                
            } else {
                $newBlocksData[$apiCode]['hasList']   = $hasList;
                $newBlocksData[$apiCode]['listField'] = $listField;
                
                if ($hasList) {
                    $newBlocksData[$apiCode]['data'] = $data[$listField];
                } else {
                    $newBlocksData[$apiCode]['data'] = $data;
                    
                }
            }
        }
        
        $subResult = [];
        foreach ($newBlocksData as $apiCode => $newBlocksDataItem) {
            $subData = $newBlocksDataItem['hasList'] ? [$newBlocksDataItem['listField'] => $newBlocksDataItem['data']] : $newBlocksDataItem['data'];
            
            $subResult[$apiCode] = $this->makeFiles($subData, $apiCode, $newBlocksDataItem['hasList'], $newBlocksDataItem['listField'] ?? null);
            $this->appendToFactory(lcfirst($apiCode));
        }
        
        $this->factoryDetector->getFactoryContent()->save();
        
        return $subResult;
    }
    
    private function makeFiles(
        array $paramBlockData,
        string $entityApiCode,
        bool $hasList,
        ?string $listField = null
    ): array {
        $result = [];
        
        if (array_key_exists('_template', $paramBlockData) && count($paramBlockData) === 1) {
            $result[] = $paramBlockData['_template'] . ' - блок пуст.';
            return $result;
        }
        
        $dt = new DateTime('now', new DateTimeZone('UTC'));
        
        $datePrefix = (int)$dt->format('YmdHis');
        
        $datePrefix += Storage::tick();
        
        $nameList = [
            'handler'    => [
                'cond' => [
                    'item' => [
                        'templatePath' => 'handleritem.php',
                        'filePath'     => 'apps/back/local/modules/pochtabank.pages/lib/service/block/#LOWER_NAME#',
                        'name'         => '#LOWER_NAME#handler.php',
                    ],
                    'list' => [
                        'templatePath' => 'handlerlist.php',
                        'filePath'     => 'apps/back/local/modules/pochtabank.pages/lib/service/block/#LOWER_NAME#',
                        'name'         => '#LOWER_NAME#handler.php',
                    ],
                ],
            ],
            'repository' => [
                'cond' => [
                    'item' => [
                        'templatePath' => 'repositoryitem.php',
                        'filePath'     => 'apps/back/local/modules/pochtabank.pages/lib/service/block/#LOWER_NAME#',
                        'name'         => '#LOWER_NAME#repository.php',
                    ],
                    'list' => [
                        'templatePath' => 'repositorylist.php',
                        'filePath'     => 'apps/back/local/modules/pochtabank.pages/lib/service/block/#LOWER_NAME#',
                        'name'         => '#LOWER_NAME#repository.php',
                    ],
                ],
            ],
            'table'      => [
                'cond' => [
                    'item' => [
                        'templatePath' => 'tableobject.php',
                        'filePath'     => 'apps/back/local/modules/pochtabank.pages/lib/orm/element',
                        'name'         => '#LOWER_NAME#table.php',
                    ],
                    'list' => [
                        'templatePath' => 'tablecollection.php',
                        'filePath'     => 'apps/back/local/modules/pochtabank.pages/lib/orm/element',
                        'name'         => '#LOWER_NAME#table.php',
                    ],
                ],
            ],
            'dto'        => [
                'cond' => [
                    'item' => [
                        'templatePath' => 'dtoitem.php',
                        'filePath'     => 'apps/back/local/modules/pochtabank.pages/lib/service/block/#LOWER_NAME#',
                        'name'         => '#LOWER_NAME#.php',
                    ],
                    'list' => [
                        'templatePath' => 'dtoitem.php',
                        'filePath'     => 'apps/back/local/modules/pochtabank.pages/lib/service/block/#LOWER_NAME#',
                        'name'         => '#LOWER_NAME#.php',
                    ],
                ],
            ],
            'dtoList'    => [
                'cond' => [
                    'list' => [
                        'templatePath' => 'dtolist.php',
                        'filePath'     => 'apps/back/local/modules/pochtabank.pages/lib/service/block/#LOWER_NAME#',
                        'name'         => '#LOWER_NAME#list.php',
                    ],
                ],
            ],
            'migration'  => [
                'cond' => [
                    'item' => [
                        'templatePath' => 'iblock_migration.php',
                        'filePath'     => 'apps/back/db/migrations',
                        'name'         => $datePrefix . '_create_iblock_#FILE_CODE_NAME#.php',
                    ],
                    'list' => [
                        'templatePath' => 'iblock_migration.php',
                        'filePath'     => 'apps/back/db/migrations',
                        'name'         => $datePrefix . '_create_iblock_#FILE_CODE_NAME#.php',
                    ],
                ],
            ],
        ];
        
        $loverName      = Helper::getLowerName($entityApiCode);
        $nameTranslator = (new NameTranslator($entityApiCode));
        foreach ($nameList as $entity => $item) {
            
            $data = $item['cond'][$hasList ? 'list' : 'item'] ?? false;
            if (!$data) {
                continue;
            }
            
            $filePath  = str_replace('#LOWER_NAME#', $loverName, $data['filePath']);
            $fileName  = str_replace(['#LOWER_NAME#', '#FILE_CODE_NAME#'], [$loverName, $nameTranslator->getFileCodeName()], $data['name']);
            $blockName = lcfirst($entityApiCode);
            
            $entityName      = Storage::getTinaBlocks()[$blockName]->getTitle() ?: $entityApiCode;
            $templateContent = file_get_contents($this->templatePath . $data['templatePath']);
            
            $templateContentReady = str_replace(['#ENTITY_API_CODE#', '#ENTITY_CODE#', '#ENTITY_NAME#'], [$entityApiCode, $nameTranslator->getStyleCodeName(), $entityName], $templateContent);
            
            $contentReplacer = new ContentReplacer($templateContentReady, json_encode($paramBlockData), $blockName, $hasList, $listField);
            
            $templateContentReady = $contentReplacer->replace();
            
            Helper::makeDirectories($this->generatePath . $filePath);
            if (file_exists($this->generatePath . $filePath . '/' . $fileName)) {
                $result[] = $entity . ' ' . $this->generatePath . $filePath . '/' . $fileName . ' уже существует.';
            } else {
                file_put_contents($this->generatePath . $filePath . '/' . $fileName, $templateContentReady);
                $result[] = $entity . ' ' . $this->generatePath . $filePath . '/' . $fileName . ' успешно создан.';
            }
            
            
        }
        
        return $result;
    }
    
    private function appendToFactory(string $blockName): void
    {
        $className = ucfirst($blockName) . 'Handler';
        
        $use = 'use Pochtabank\Pages\Service\Block\\' . ucfirst($blockName) . '\\' . $className . ';';
        
        $class = "return new $className()";
        
        $block = "case '$blockName':\n                " . $class;
        
        $content = $this->factoryDetector->getFactoryContent()->getContent();
        
        $content = preg_replace('/(;[\n\t\s]+default:)/is', ";\n            $block$1", $content);
        $content = preg_replace('/(use(.+?);)([\n\t\s]+)class\sBlockFactory/siu', "use$2;\n$use$3class BlockFactory", $content);
        
        $this->factoryDetector->getFactoryContent()->setContent($content);
    }
}
