<?php

namespace Tools\Detectors;

use Tools\IFactoryDetector;
use Tools\ISimpleFileDetector;

class BlockDetector implements IFactoryDetector
{
    /**
     * @var string
     */
    private $projectPath;
    /**
     * @var string
     */
    private $savePath;
    
    private const SERVICE_PATH = '/apps/back/local/modules/pochtabank.pages/lib';
    /**
     * @var BlockDetected[]
     */
    private $instances;
    /**
     * @var FileDetector
     */
    private $factory;
    
    public function __construct(string $projectPath, string $savePath)
    {
        $this->projectPath = $projectPath;
        $this->savePath    = $savePath;
        $this->instances   = [];
        
        $this->readFactory();
        
    }
    
    public function detect(string $template): BlockDetected
    {
        if (!array_key_exists($template, $this->instances)) {
            $this->instances[$template] = new BlockDetected(
                $this->projectPath,
                $this->savePath,
                self::SERVICE_PATH,
                $template,
                $this->factory->getContent()
            );
        }
        
        return $this->instances[$template];
    }
    
    public function getFactoryContent(): ISimpleFileDetector
    {
        return $this->factory;
    }
    
    private function readFactory(): void
    {
        $relativePath = self::SERVICE_PATH . '/service/block/blockfactory.php';
        $absolutePath = $this->projectPath . $relativePath;
        
        $factory = file_get_contents($absolutePath);
        
        $this->factory = new SimpleFileDetector($this->savePath . $relativePath, $factory);
    }
}
