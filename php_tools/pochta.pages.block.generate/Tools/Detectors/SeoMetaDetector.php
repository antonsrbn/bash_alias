<?php

namespace Tools\Detectors;

class SeoMetaDetector
{
    /**
     * @var array
     */
    private $meta;
    /**
     * @var string
     */
    private $route;
    
    public function __construct(array $meta, string $route)
    {
        $this->meta  = $meta;
        $this->route = $route;
    }
    
    public function detect(): self
    {
        $content = file_get_contents('https://www.pochtabank.ru/' . $this->route);
        $matches = [];
        preg_match_all('/<title>(.+?)<\/title>/', $content, $matches);
        $title = $matches[1][0] ?: null;
        
        if ($title === 'Ошибка 404') {
            return $this;
        }
        
        $matches = [];
        preg_match_all('/<meta\s+name="description"\s+content="(.+?)"\s*\/>/', $content, $matches);
        $description = $matches[1][0] ?: null;
        
        $matches = [];
        preg_match_all('/<meta\s+name="keywords"\s+content="(.+?)"\s*\/>/', $content, $matches);
        $keywords = $matches[1][0] ?: null;
        
        $matches = [];
        preg_match_all('/<meta\s+property="og:title"\s+content="(.+?)"\s*>/', $content, $matches);
        $ogTitle = $matches[1][0] ?: null;
        
        $matches = [];
        preg_match_all('/<meta\s+property="og:image"\s+content="(.+?)"\s*>/', $content, $matches);
        $ogImage = $matches[1][0] ?: null;
        
        $matches = [];
        preg_match_all('/<meta\s+property="og:description"\s+content="(.+?)"\s*>/', $content, $matches);
        $ogDescription = $matches[1][0] ?: null;
        
        $this->meta['title']         = $title ?: $this->meta['title'];
        $this->meta['description']   = $description ?: $this->meta['description'];
        $this->meta['keywords']      = $keywords ?: $this->meta['keywords'];
        $this->meta['ogTitle']       = $ogTitle ?: $this->meta['ogTitle'];
        $this->meta['ogImage']       = $ogImage ?: $this->meta['ogImage'];
        $this->meta['ogDescription'] = $ogDescription ?: $this->meta['ogDescription'];
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }
    
}
