<?php

namespace Tools\Detectors;

use Tools\FieldParser;
use Tools\FieldsFactory\JsonField;
use Tools\NameTranslator;

class BlockTypeDetector
{
    /**
     * @var array
     */
    private $blockData;
    /**
     * @var FieldParser
     */
    private $fieldParser;
    /**
     * @var bool
     */
    private $hasList;
    /**
     * @var string
     */
    private $listField;
    
    public function __construct(array $blockData)
    {
        $this->blockData   = $blockData;
        $this->fieldParser = new FieldParser();
        $this->hasList     = false;
        $this->listField   = '';
        
        $this->calculate();
    }
    
    public function hasList(): bool
    {
        return $this->hasList;
    }
    
    /**
     * @return string
     */
    public function getListField(): string
    {
        return $this->listField;
    }
    
    private function calculate(): void
    {
        $jsonFiends = [];
        $types      = $this->fieldParser->getFieldTypes($this->blockData, $this->blockData['_template']);
        
        $countIterableFields    = 0;
        $countNotIterableFields = 0;
        $countAllFields         = count($types);
        
        foreach ($types as $key => $type) {
            if ($type instanceof JsonField) {
                $jsonFiends[$key] =
                    [
                        'iterable'    => true,
                        'count'       => count($type->getValue()),
                        'countFields' => 0,
                        'fields'      => [],
                    ];
                $fields           = [];
                foreach ($type->getValue() as $subKey => $item) {
                    if (is_string($subKey) || !is_array($item)) {
                        $jsonFiends[$key]['iterable'] = false;
                        break;
                    }
                    
                    /** @noinspection SlowArrayOperationsInLoopInspection */
                    $fields = array_merge($fields, array_keys($item));
                }
                
                $fields                          = array_unique($fields);
                $jsonFiends[$key]['countFields'] = count($fields);
                $jsonFiends[$key]['fields']      = $fields;
                
                if ($jsonFiends[$key]['iterable']) {
                    $this->listField = $key;
                    $countIterableFields++;
                } else {
                    $countNotIterableFields++;
                }
                
            }
        }
        
        $this->hasList = $this->verdict($countAllFields, $countIterableFields, $countNotIterableFields);
    }
    
    private function verdict(int $countAllFields, int $countIterableFields, int $countNotIterableFields): bool
    {
        $simpleFields = $countAllFields - ($countIterableFields + $countNotIterableFields);
        
        if ($countIterableFields === 1) {
            return ($simpleFields / $countAllFields <= 0.8);
        }
        
        return false;
    }
    
}
