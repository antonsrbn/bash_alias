<?php

namespace Tools\Detectors;

use Tools\Blocks\IFileDetector;

class FileDetector extends SimpleFileDetector implements IFileDetector
{
    /**
     * @var BlockDetector
     */
    private $blockDetected;
    
    public function __construct(string $savePath, string $content, BlockDetected $blockDetected)
    {
        parent::__construct($savePath, $content);
        $this->blockDetected = $blockDetected;
    }
    
    public function context(): BlockDetected
    {
        return $this->blockDetected;
    }
}
