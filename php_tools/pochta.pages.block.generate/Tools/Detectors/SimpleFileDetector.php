<?php

namespace Tools\Detectors;

use Tools\ISimpleFileDetector;

class SimpleFileDetector implements ISimpleFileDetector
{
    /**
     * @var string
     */
    private $savePath;
    /**
     * @var string
     */
    private $content;
    
    public function __construct(string $savePath, string $content)
    {
        $this->savePath      = $savePath;
        $this->content       = $content;
    }
    
    public function getContent(): string
    {
        return $this->content;
    }
    
    public function setContent(string $content): void
    {
        $this->content = $content;
    }
    
    public function save(): bool
    {
        return file_put_contents($this->savePath, $this->content);
    }
}
