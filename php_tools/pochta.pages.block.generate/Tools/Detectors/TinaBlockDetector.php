<?php

namespace Tools\Detectors;

use Symfony\Component\VarDumper\VarDumper;
use Tools\Detectors\DTO\TinaBlock;
use Tools\Detectors\DTO\TinaProp;
use Tools\FieldsFactory\BooleanField;
use Tools\FieldsFactory\FileField;
use Tools\FieldsFactory\StringField;
use Tools\FieldsFactory\TextField;
use OviDigital\JsObjectToJson\JsConverter;
use Tools\Tsx\Parser;

/**
 * Class TinaBlockDetector.
 *
 * @package Tools\Detectors
 */
class TinaBlockDetector
{
    /**
     * @var string
     */
    private $blocksPath;
    /**
     * @var array
     */
    private $tinaConfig;
    /**
     * @var string
     */
    private $frontPath;
    /**
     * @var array
     */
    private $failList;
    
    public function __construct(string $frontPath)
    {
        $this->blocksPath = $frontPath . '/tina/blocks';
        $this->frontPath  = $frontPath;
        
        $this->parseTinaConfig($frontPath . '/tina/config/index.tina.config.tsx');
        
    }
    
    /**
     * @param string[] $blockNames
     * @return TinaBlock[]
     */
    public function detect(array $blockNames): array
    {
        $blockNames = array_unique($blockNames);
        $tinaBlocks = [];
        
        $fail = [];
        foreach ($blockNames as $blockName) {
            $block                  = $this->detectTinaBlock($blockName);
            $tinaBlocks[$blockName] = $block;
            $errors                 = [];
            if (!$block->isHasExists()) {
                $errors[] = 'Не удалось найти блок. Возможно он не подключен к Tina';
            } else {
                if (!$block->getTitle()) {
                    $errors[] = 'Не удалось получить заголовок блока';
                }
                
                if (!$block->getProps()) {
                    $errors[] = 'Не удалось получить свойства блока';
                }
            }
            
            if ($errors) {
                $fail[$blockName] = $errors;
            }
            
        }
        $this->failList = $fail;
        return $tinaBlocks;
    }
    
    private function detectTinaBlock(string $blockName): TinaBlock
    {
        $tinaBlock = new TinaBlock();
        
        $blockContent = $this->getBlockContent($blockName);
        $tinaBlock->setHasExists((bool)$blockContent);
        
        $matches = [];
        preg_match_all('/export\sconst\s' . $blockName . 'Block\s=\s\{(.+?)template:(.+?)\{(.+?)label:(.+?)\'(.+?)\'/si', $blockContent, $matches);
        
        $blockTitle = $matches[5][0] ?? null;
        
        $tinaBlock->setTitle($blockTitle);
        
        $matches = [];
        
        preg_match_all('/export\sconst\s' . $blockName . 'Block\s=\s\{(.+?)template:(.+?)\{(.+?)fields:(.+?)\[(.+)\],(.+?)\},(.+?)\};/si', $blockContent, $matches);
        
        $properties = $matches[5][0] ?? [];
        /*if ($blockName === 'textBlock') {
              dd($properties);
        }*/
        
        $callable = static function ($matches) use ($blockName) {
            /* if($blockName==='ourTargets' && str_replace('half','',$matches[2])!==$matches[2]){
                 dd($matches[2]);
             }*/
            if (preg_replace('/^\'(.+?)$/si', '', $matches[2]) !== $matches[2]) {
                if ($blockName === 'textBlock') {
                    //  var_dump($matches);
                }
                return $matches[1] . ': ' . $matches[2] . $matches[3] . $matches[4];
            }
            if (preg_replace('/^([\n\t\r\s])*(\[|\{)/si', '', $matches[2]) !== $matches[2]) {
                return $matches[1] . ': ' . $matches[2] . $matches[3] . $matches[4];
            }
            if (preg_replace('/^(true|false|null)/si', '', $matches[2]) !== $matches[2]) {
                return $matches[1] . ': ' . $matches[2] . $matches[3] . $matches[4];
            }
            if (preg_replace('/^(\d)/si', '', $matches[2]) !== $matches[2]) {
                return $matches[1] . ': ' . $matches[2] . $matches[3] . $matches[4];
            }
            if (preg_replace('/^(-\d)/si', '', $matches[2]) !== $matches[2]) {
                return $matches[1] . ': ' . $matches[2] . $matches[3] . $matches[4];
            }
            
            return null;
            
        };
        
        $callableTwo = static function ($matches) use ($blockName) {
            /* if($blockName==='ourTargets' && str_replace('half','',$matches[2])!==$matches[2]){
                 dd($matches[2]);
             }*/
            if (preg_replace('/^\'(.+?)\'$/si', '', $matches[2]) !== $matches[2]) {
                if ($blockName === 'textBlock') {
                    //  var_dump($matches);
                }
                return $matches[1] . ': ' . $matches[2] . $matches[3];
            }
            if (preg_replace('/^([\n\t\r\s])*(\[|\{)/si', '', $matches[2]) !== $matches[2]) {
                return $matches[1] . ': ' . $matches[2] . $matches[3];
            }
            if (preg_replace('/^(true|false|null)/si', '', $matches[2]) !== $matches[2]) {
                return $matches[1] . ': ' . $matches[2] . $matches[3];
            }
            if (preg_replace('/^(\d)/si', '', $matches[2]) !== $matches[2]) {
                return $matches[1] . ': ' . $matches[2] . $matches[3];
            }
            if (preg_replace('/^(-\d)/si', '', $matches[2]) !== $matches[2]) {
                return $matches[1] . ': ' . $matches[2] . $matches[3];
            }
            
            return null;
            
        };
        $data        = [];
        $result      = preg_replace_callback('/(.+?):[\s\n\t]+(.+?)(\'|\d|true|false|null|\}|\])(,|\s*\},)/si', $callable, $properties);
        $resultTwo   = preg_replace_callback('/(.+?):[\s\n\t]+(.+?)(,|\s*\},)/si', $callableTwo, $properties);
        
        if ($result) {
            $data = $this->trasformToJsonArray($result);
        }
        
        if (!$data && $resultTwo) {
            $data = $this->trasformToJsonArray($resultTwo);
        }
        
        $props = $this->treeTyperator($data);
        /*
        foreach ($data as $property) {
            $keyField = $property['name'];
            
            if ($keyField === null) {
                continue;
            }
            
            $keyType = $property['component'];
            
            $keyTitle = $property['label'];
            
            $props[$keyField] = (new TinaProp())->setTitle($keyTitle)->setTypeClassName($this->checkType($keyType))->setBody($property);
        }*/
        
        $tinaBlock->setProps($props);
        
        return $tinaBlock;
    }
    
    private function treeTyperator(array $data): array
    {
        $props = [];
        foreach ($data as $property) {
            $keyField = $property['name'];
            $fields   = null;
            if (array_key_exists('fields', $property)) {
                $fields = $this->treeTyperator($property['fields']);
            }
            
            $keyType = $property['component'];
            
            $keyTitle = $property['label'];
            
            $props[$keyField] = (new TinaProp()
            )
                ->setTitle($keyTitle)
                ->setTypeClassName($this->checkType($keyType))
                ->setBody($property)
                ->setProps($fields);
        }
        
        return $props;
    }
    
    
    private function removeFatalDelimiters(string $superContent): string
    {
        $superContent = preg_replace('/(,)([\n\t\s]{0,})([\]\}])/s', '$2$3', $superContent);
        $superContent = preg_replace('/\'[\n\t\s]{0,}\+[\n\t\s]{0,}\'/ui', '', $superContent);
        
        return preg_replace_callback('/(.+?):\s\`(.+?)\`/s', static function ($matches) {
            return $matches[1] . ': \'' . preg_replace('/\n/', '', $matches[2]) . '\'';
        }, $superContent);
    }
    
    private function trasformToJsonArray(string $data): array
    {
        $superContent = $this->removeFatalDelimiters('[' . $data . ']');
        
        $json = JsConverter::convertToJson($superContent);
        
        $json = str_replace([
            '"true"',
            '"false"',
            '"null"',
        ], [
            'true',
            'false',
            'null',
        ], $json);
        
        return json_decode($json, true) ?? [];
    }
    
    private function checkType(string $keyType): ?string
    {
        switch ($keyType) {
            case 'text':
            case 'title':
            case 'html':
                return StringField::class;
            case 'textarea':
                return TextField::class;
            case 'image':
                return FileField::class;
            default:
                return null;
        }
    }
    
    /**
     * @param string $blockName
     * @return string|null
     */
    private function getBlockContent(string $blockName): ?string
    {
        $fromTina = $this->tinaConfig['blocks'][$blockName] ?? null;
        
        if ($fromTina && file_exists($fromTina)) {
            return file_get_contents($fromTina);
        }
        
        $filePath = $this->blocksPath . '/' . $blockName . '/index.tsx';
        
        if (file_exists($filePath)) {
            return file_get_contents($filePath);
        }
        
        return null;
    }
    
    private function parseTinaConfig(string $path): void
    {
        $blockPaths = [];
        $content    = file_get_contents($path);
        $matches    = [];
        preg_match_all('/(.+?):\s*(.+?),/ui', $content, $matches);
        $aliases = [];
        $list    = $matches[0];
        foreach ($list as $item) {
            $expl                    = explode(':', str_replace(',', '', $item));
            $aliases[trim($expl[0])] = trim($expl[1]);
        }
        
        $imports = Parser::getImports($this->frontPath, $content);
        foreach ($aliases as $template => $import) {
            $blockPaths[$template] = $imports[$import] ?? null;
        }
        
        $this->tinaConfig['blocks'] = $blockPaths;
    }
    
    /**
     * @return array
     */
    public function getFailList(): array
    {
        return $this->failList;
    }
    
    
}
