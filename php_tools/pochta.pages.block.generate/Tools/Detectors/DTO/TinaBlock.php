<?php

namespace Tools\Detectors\DTO;

/**
 * Class TinaBlock.
 */
class TinaBlock
{
    /**
     * @var string|null
     */
    private $title;
    /**
     * @var TinaProp[]
     */
    private $props;
    /**
     * @var bool
     */
    private $hasExists;
    
    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }
    
    /**
     * @return TinaProp[]
     */
    public function getProps(): array
    {
        return $this->props;
    }
    
    /**
     * @param TinaProp[] $props
     * @return $this
     */
    public function setProps(array $props): self
    {
        $this->props = $props;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isHasExists(): bool
    {
        return $this->hasExists;
    }
    
    /**
     * @param bool $hasExists
     * @return $this
     */
    public function setHasExists(bool $hasExists): self
    {
        $this->hasExists = $hasExists;
        return $this;
    }
    
}
