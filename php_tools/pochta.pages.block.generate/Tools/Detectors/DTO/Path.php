<?php

namespace Tools\Detectors\DTO;

class Path
{
    /**
     * @var string
     */
    private $relativePath;
    /**
     * @var string
     */
    private $absolutePath;
    
    public function __construct(string $relativePath, string $absolutePath)
    {
        
        $this->relativePath = $relativePath;
        $this->absolutePath = $absolutePath;
    }
    
    /**
     * @return string
     */
    public function getRelativePath(): string
    {
        return $this->relativePath;
    }
    
    /**
     * @return string
     */
    public function getAbsolutePath(): string
    {
        return $this->absolutePath;
    }
}
