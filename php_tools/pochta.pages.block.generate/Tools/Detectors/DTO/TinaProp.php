<?php

namespace Tools\Detectors\DTO;

/**
 * Class TinaProp.
 */
class TinaProp
{
    /**
     * @var string|null
     */
    private $title;
    /**
     * @var string|null
     */
    private $typeClassName;
    
    /**
     * @var array
     */
    private $body;
    /**
     * @var TinaProp[]|null
     */
    private $props;
    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
    
    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getTypeClassName(): ?string
    {
        return $this->typeClassName;
    }
    
    /**
     * @param string|null $typeClassName
     * @return $this
     */
    public function setTypeClassName(?string $typeClassName): self
    {
        $this->typeClassName = $typeClassName;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }
    
    /**
     * @param array $body
     * @return $this
     */
    public function setBody(array $body): self
    {
        $this->body = $body;
        return $this;
    }
    
    /**
     * @return TinaProp[]|null
     */
    public function getProps(): ?array
    {
        return $this->props;
    }
    
    /**
     * @param TinaProp[]|null $props
     * @return $this
     */
    public function setProps(?array $props): self
    {
        $this->props = $props;
        return $this;
    }
    
}
