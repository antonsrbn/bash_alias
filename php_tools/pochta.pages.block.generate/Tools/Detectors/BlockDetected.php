<?php

namespace Tools\Detectors;

use Tools\Blocks\IBlockContentDetector;
use Tools\Blocks\IFileDetector;
use Tools\Detectors\DTO\Path;

class BlockDetected implements IBlockContentDetector
{
    /**
     * @var string
     */
    private $projectPath;
    /**
     * @var string
     */
    private $savePath;
    /**
     * @var string
     */
    private $blockName;
    /**
     * @var false
     */
    private $isList;
    /**
     * @var bool
     */
    private $detectTypeSuccess;
    /**
     * @var false
     */
    private $detectEntitySuccess;
    /**
     * @var string
     */
    private $entityApiCode;
    /**
     * @var bool
     */
    private $detectIterableField;
    /**
     * @var string
     */
    private $iterableField;
    /**
     * @var false
     */
    private $blockExists;
    /**
     * @var IFileDetector[]
     */
    private $fileDetectors;
    /**
     * @var string
     */
    private $servicePath;
    /**
     * @var string
     */
    private $factoryContent;
    
    public function __construct(
        string $projectPath,
        string $savePath,
        string $servicePath,
        string $blockName,
        string $factoryContent
    )
    {
        $this->projectPath = $projectPath;
        $this->savePath    = $savePath;
        $this->blockName   = $blockName;
        $this->factoryContent = $factoryContent;
        
        $this->fileDetectors = [];
        $this->blockExists   = false;
    
        $this->detectTypeSuccess = false;
        $this->isList            = false;
    
        $this->detectEntitySuccess = false;
        $this->entityApiCode       = '';
    
        $this->detectIterableField = false;
        $this->iterableField       = '';
        $this->servicePath         = $servicePath;
    
        $this->detect();
    }
    
    private function detect(): void
    {
        $matches = [];
        preg_match_all('/case\s*\'' . $this->blockName . '\'\s*:(.+?)return\s*new\s*(.+?)\(\);/s',  $this->factoryContent, $matches);
        
        $classNameBlockHandler = $matches[2][0] ?? null;
        
        if (!$classNameBlockHandler) {
            return;
        }
        
        $this->blockExists = true;
        
        $filePathHandler = $this->getFilePathByClassName($classNameBlockHandler, $this->factoryContent);
        
        if (!file_exists($filePathHandler->getAbsolutePath())) {
            return;
        }
        
        $handler = file_get_contents($filePathHandler->getAbsolutePath());
        
        $matches = [];
        preg_match_all('/modifyBlockData(.+?)new\s(.+?)Repository/is', $handler, $matches);
        
        $repositoryClassName = $matches[2][0] ?? null;
        
        if (!$repositoryClassName) {
            return;
        }
        
        $repositoryClassName .= 'Repository';
        
        $filePathRepository = $this->getFilePathByClassName($repositoryClassName, $handler);
        
        if (!file_exists($filePathRepository->getAbsolutePath())) {
            return;
        }
        
        $repository = file_get_contents($filePathRepository->getAbsolutePath());
        $this->makeFileDetector($repository, $filePathRepository, 'repository');
        
        $matches = [];
        preg_match_all('/class\s+' . $repositoryClassName . '\s+extends\s+(BasicListItemRepository|BasicListRepository)/i', $repository, $matches);
        
        $repositoryExtends = $matches[1][0] ?? null;
        
        switch ($repositoryExtends) {
            case 'BasicListItemRepository':
                $this->isList            = false;
                $this->detectTypeSuccess = true;
                break;
            case  'BasicListRepository':
                $this->isList            = true;
                $this->detectTypeSuccess = true;
                break;
            default:
                $this->detectTypeSuccess = false;
        }
        
        if ($this->isList) {
            $matches = [];
            preg_match_all('/\$(.+?)\[\'(.+?)\'\]\s*=\s*(.+?)new(.+?)Repository/i', $handler, $matches);
            
            $iterableField = $matches[2][0] ?? null;
            if (!$iterableField) {
                $this->detectIterableField = true;
                $this->iterableField       = $iterableField;
            }
        }
        
        $matches = [];
        preg_match_all('/getTableClass\(\):(.+?)return\s+(.+?)::class\s*;/s', $repository, $matches);
        $tableEntityClassName = $matches[2][0] ?? null;
        
        if (!$tableEntityClassName) {
            return;
        }
        $filePathTableEntity = $this->getFilePathByClassName($tableEntityClassName, $repository);
        
        if (!file_exists($filePathTableEntity->getAbsolutePath())) {
            return;
        }
        
        $tableEntity = file_get_contents($filePathTableEntity->getAbsolutePath());
        $this->makeFileDetector($tableEntity, $filePathTableEntity, 'table');
        
        $matches = [];
        preg_match_all('/class\s+' . $tableEntityClassName . '\s+extends\s+Element(.+?)Table/i', $tableEntity, $matches);
        
        $entityApiCode = $matches[1][0] ?? null;
        
        if (!$entityApiCode) {
            return;
        }
        
        $this->entityApiCode       = $entityApiCode;
        $this->detectEntitySuccess = true;
        
        if ($repository) {
            $this->detectDto($repository);
        }
    }
    
    
    private function getFilePathByClassName(string $className, string $content): Path
    {
        $matches = [];
        preg_match_all('/use\sPochtabank\\\Pages\\\+(.+?)' . $className . ';/ui', $content, $matches);
        
        $namespace = $matches[1][0] ?? null;
        
        if (!$namespace) {
            $matches = [];
            preg_match_all('/namespace\sPochtabank\\\Pages\\\+(.+?);/ui', $content, $matches);
            $namespace = $matches[1][0] . '\\';
        }
        
        $filePath = strtolower($namespace . $className);
        
        $relativePath = $this->servicePath . '/' . str_replace('\\', '/', $filePath) . '.php';
        
        $absolutePath = $this->projectPath . $relativePath;
        
        return new Path($relativePath, $absolutePath);
    }
    
    /**
     * @return false
     */
    public function getIsList(): bool
    {
        return $this->isList;
    }
    
    /**
     * @return bool
     */
    public function isDetectTypeSuccess(): bool
    {
        return $this->detectTypeSuccess;
    }
    
    /**
     * @return false
     */
    public function getDetectEntitySuccess(): bool
    {
        return $this->detectEntitySuccess;
    }
    
    /**
     * @return string
     */
    public function getEntityApiCode(): string
    {
        return $this->entityApiCode;
    }
    
    /**
     * @return bool
     */
    public function isDetectIterableField(): bool
    {
        return $this->detectIterableField;
    }
    
    /**
     * @return string
     */
    public function getIterableField(): string
    {
        return $this->iterableField;
    }
    
    /**
     * @return false
     */
    public function getBlockExists(): bool
    {
        return $this->blockExists;
    }
    
    
    private function detectDto(string $repository): void
    {
        $matches = [];
        preg_match_all('/(getListItemClass|getListClass)\(\):(.+?)return\s+(.+?)::class\s*;/s', $repository, $matches);
        $dtoClassName = $matches[3][0] ?? null;
        
        if ($dtoClassName) {
            $fileDto = $this->getFilePathByClassName($dtoClassName, $repository);
            
            if (!file_exists($fileDto->getAbsolutePath())) {
                return;
            }
            
            $dto = file_get_contents($fileDto->getAbsolutePath());
            if (!$this->isList) {
                $this->makeFileDetector($dto, $fileDto, 'dto');
            } else {
                $matches = [];
                preg_match_all('/getItemClassName\(\):(.+?)return\s+(.+?)::class\s*;/s', $dto, $matches);
                $dtoClassName = $matches[2][0] ?? null;
                if ($dtoClassName) {
                    $fileDto = $this->getFilePathByClassName($dtoClassName, $dto);
                    if (!file_exists($fileDto->getAbsolutePath())) {
                        return;
                    }
                    
                    $dto = file_get_contents($fileDto->getAbsolutePath());
                    $this->makeFileDetector($dto, $fileDto, 'dto');
                }
                
                
            }
        }
    }
    
    public function getRepositoryContent(): ?IFileDetector
    {
        return $this->fileDetectors['repository'] ?? null;
    }
    
    public function getTableContent(): ?IFileDetector
    {
        return $this->fileDetectors['table'] ?? null;
    }
    
    public function getDtoContent(): ?IFileDetector
    {
        return $this->fileDetectors['dto'] ?? null;
    }
    
    public function makeFileDetector(string $content, Path $path, string $type): void
    {
        $this->fileDetectors[$type] = new FileDetector($this->savePath . $path->getRelativePath(), $content, $this);
    }
}
