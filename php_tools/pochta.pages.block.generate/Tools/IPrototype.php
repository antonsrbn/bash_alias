<?php

namespace Tools;

interface IPrototype
{
    public function selfClone($self);
}
