<?php

namespace Tools;

interface IFactoryDetector
{
    public function getFactoryContent(): ISimpleFileDetector;
}
