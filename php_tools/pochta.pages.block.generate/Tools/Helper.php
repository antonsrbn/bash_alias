<?php

namespace Tools;

use RuntimeException;

class Helper
{
    public static function smartCopyFile(string $from, string $to): bool
    {
        $paths = explode('/', $to);
        
        array_pop($paths);
        
        self::makeDirectories(implode('/', $paths));
        
        return copy($from, $to);
    }
    
    public static function makeDirectories(string $path): void
    {
        $uploadDirectories = explode('/', $path);
        
        if (!$uploadDirectories[0]) {
            array_shift($uploadDirectories);
        }
        
        if (!$uploadDirectories[count($uploadDirectories) - 1]) {
            array_pop($uploadDirectories);
        }
        
        $createDirectory = [];
        foreach ($uploadDirectories as $uploadDirectory) {
            $createDirectory[]   = $uploadDirectory;
            $createDirectoryPath = '/' . implode('/', $createDirectory);
            
            if (!is_dir($createDirectoryPath) && !mkdir($createDirectoryPath, 0777, true) && !is_dir($createDirectoryPath)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $createDirectoryPath));
            }
        }
    }
    
    public static function getLowerName(string $apiCode): string
    {
        $toLowerCase = mb_strtolower($apiCode);
        $toLowerCase = trim(preg_replace('/\s/ui', '', $toLowerCase));
        
        $toLowerCase = preg_replace('/[^\dA-Za-z]/ui', '', $toLowerCase);
        
        return preg_replace('/^\d/ui', '', $toLowerCase);
    }
    
    public static function array_merge_recursive_distinct()
    {
        $arrays = func_get_args();
        $base = array_shift($arrays);
        if (!is_array($base)) {
            $base = empty($base) ? array() : array($base);
        }
        foreach ($arrays as $append) {
            if (!is_array($append)) {
                $append = array($append);
            }
            foreach ($append as $key => $value) {
                if (!array_key_exists($key, $base) and !is_numeric($key)) {
                    $base[$key] = $append[$key];
                    continue;
                }
                if (is_array($value) or is_array($base[$key])) {
                    $base[$key] = self::array_merge_recursive_distinct($base[$key], $append[$key]);
                } else {
                    if (is_numeric($key)) {
                        if (!in_array($value, $base)) {
                            $base[] = $value;
                        }
                    } else {
                        $base[$key] = $value;
                    }
                }
            }
        }
        return $base;
    }
}
