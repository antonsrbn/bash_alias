<?php

namespace Tools;

interface ISimpleFileDetector
{
    public function getContent(): string;
    
    public function setContent(string $content): void;
    
    public function save(): bool;
}
