<?php

namespace Tools\Tsx;

use OviDigital\JsObjectToJson\JsConverter;
use RuntimeException;
use Symfony\Component\VarDumper\VarDumper;

class Parser
{
    /**
     * @var string
     */
    private $filePath;
    /**
     * @var array
     */
    private $data;
    
    /**
     * @param string $filePath
     * @param string $route
     */
    public function __construct(string $filePath, string $route)
    {
        $this->filePath   = $filePath;
        $mainMockFilePath = $filePath . '/pages/mock/' . $route . '/index.tsx';
        $mainMockContent  = file_get_contents($mainMockFilePath);
        $mockContent      = $this->getPageData($mainMockContent);
        
        $superContent = $this->generateSuperContent($mockContent);
        $superContent = $this->removeFatalDelimiters($superContent);
        $json         = JsConverter::convertToJson($superContent);
        
        $json = str_replace([
            '"true"',
            '"false"',
            '"null"',
        ], [
            'true',
            'false',
            'null',
        ], $json);
        
        $this->data = json_decode($json, true) ?? [];
    }
    
    public function getData(): array
    {
        return $this->data;
    }
    
    public function getJson(): string
    {
        return json_encode($this->data);
    }
    
    private function getPageData(string $mainMock): array
    {
        preg_match_all('/mockStaticProps\((.+)?,\s(.+)?,\s\'(.+)?\'\)/ui', $mainMock, $matches);
        
        $var = $matches[2][0];
        
        $imports = self::getImports($this->filePath, $mainMock);
        
        $mockFilePath = null;
        foreach ($imports as $keyVar => $import) {
            if ($var === $keyVar) {
                $mockFilePath = $import;
            }
        }
        
        return ['content' => file_get_contents($mockFilePath), 'variable' => $var];
    }
    
    private function generateSuperContent(array $mockContent): string
    {
        $imports = self::getImports($this->filePath, $mockContent['content']);
        
        $content = $this->scopeJsObject($mockContent['variable'], $mockContent['content']);
        
        if ($this->hasScopes(array_keys($imports), $content)) {
            foreach ($imports as $var => $import) {
                if ($this->hasScope($var, $content)) {
                    $result = $this->generateSuperContent(['variable' => $var, 'content' => file_get_contents($import)]);
                    $scopes = $this->getScopes($var, $content);
                    
                    $content = $this->replaceByScope($scopes, $result, $content);
                }
            }
        } else {
            return $content;
        }
        
        return $content;
    }
    
    private function scopeJsObject(string $var, string $content): string
    {
        
        $matches = [];
        preg_match_all('/export\sconst\s' . $var . '(.+?)\=\s\{(.+?)\};/s', $content, $matches);
        
        return '{' . $matches[2][0] . '}';
    }
    
    public static function getImports(string $filePathParam, string $content): array
    {
        $matches = [];
        preg_match_all('/import\s\{\s(.+)?\s\}\sfrom\s\'@(.+)?\';/ui', $content, $matches);
        $result = [];
        
        foreach ($matches[1] as $key => $match) {
            $filePathIndex  = $filePathParam . $matches[2][$key] . '/index.ts';
            $filePathIndexX = $filePathParam . $matches[2][$key] . '/index.tsx';
            $filePath       = $filePathParam . $matches[2][$key] . '.ts';
            $filePathX      = $filePathParam . $matches[2][$key] . '.tsx';
            if (file_exists($filePathIndex)) {
                $result[$match] = $filePathIndex;
            }
            if (file_exists($filePathIndexX)) {
                $result[$match] = $filePathIndexX;
            }
            if (file_exists($filePath)) {
                $result[$match] = $filePath;
            }
            
            if (file_exists($filePathX)) {
                $result[$match] = $filePathX;
            }
            if (!($result[$match] ?? false)) {
                $result[$match] = null;
            }
            
        }
        
        return $result;
    }
    
    private function hasScopes(array $keys, string $content): bool
    {
        foreach ($keys as $key) {
            if ($this->hasScope($key, $content)) {
                return true;
            }
        }
        
        return false;
    }
    
    private function hasScope(string $key, string $content): bool
    {
        $has = str_replace([
                '...' . $key . ',',
                ': ' . $key . ',',
            ], '', $content) !== $content;
        
        $pregScopes = [
            '/\.\.\.\(' . $key . ' as (.+?)\),/ui',
        ];
        
        if (!$has) {
            $has = preg_replace(
                    $pregScopes,
                    '', $content) !== $content;
        }
        
        return $has;
    }
    
    private function getScopes(string $key, string $content): array
    {
        $scopes = [
            '...' . $key . ',',
            ': ' . $key . ',',
        ];
        
        $pregScopes = [
            '/\.\.\.\(' . $key . ' as (.+?)\),/ui',
        ];
        
        $scopesInject = [];
        foreach ($scopes as $scope) {
            if (str_replace($scope, '', $content) !== $content) {
                $scopesInject[] = $scope;
            }
        }
        
        foreach ($pregScopes as $scope) {
            if (preg_replace($scope, '', $content) !== $content) {
                
                $matches = [];
                preg_match_all($scope, $content, $matches);
                $scopesInject[] = $matches[0][0];
            }
        }
        
        if ($scopesInject) {
            return $scopesInject;
        }
        
        throw new RuntimeException('fail scope');
    }
    
    private function replaceByScope(array $scopes, string $in, string $content): string
    {
        foreach ($scopes as $scope) {
            if (preg_replace('/\.\.\.\((.+?) as (.+?)\),/ui', '', $scope) !== $scope) {
                $in      = trim($in);
                $in      = preg_replace(['/^{/ui', '/}$/ui'], '', $in);
                $content = str_replace($scope, $in . '', $content);
            } elseif (str_replace('...', '', $scope) !== $scope) {
                $in      = trim($in);
                $in      = preg_replace(['/^{/ui', '/}$/ui'], '', $in);
                $content = str_replace($scope, $in . '', $content);
            } else {
                $content = str_replace($scope, ': ' . $in . ',', $content);
            }
        }
        
        return $content;
    }
    
    private function removeFatalDelimiters(string $superContent): string
    {
        $superContent = preg_replace('/(,)([\n\t\s]{0,})([\]\}])/s', '$2$3', $superContent);
        $superContent = preg_replace('/\'[\n\t\s]{0,}\+[\n\t\s]{0,}\'/ui', '', $superContent);
        
        return preg_replace_callback('/(.+?):\s\`(.+?)\`/s', static function ($matches) {
            return $matches[1] . ': \'' . preg_replace('/\n/', '', $matches[2]) . '\'';
        }, $superContent);
    }
}
