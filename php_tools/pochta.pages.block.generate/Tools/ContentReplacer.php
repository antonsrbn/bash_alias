<?php

namespace Tools;

use RuntimeException;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Class FileReplacer.
 *
 * @package Tools
 */
class ContentReplacer
{
    /**
     * @var FieldParser
     */
    private $fieldParser;
    /**
     * @var mixed
     */
    private $json;
    /**
     * @var string
     */
    private $content;
    /**
     * @var bool
     */
    private $hasList;
    /**
     * @var string|null
     */
    private $listField;
    /**
     * @var string
     */
    private $blockName;
    
    
    public function __construct(string $content, string $jsonBlock, string $blockName, bool $hasList = false, ?string $listField = null)
    {
        $this->fieldParser = new FieldParser();
        
        $this->json = json_decode($jsonBlock, true);
        if ($this->json === null) {
            throw new RuntimeException('Invalid json');
        }
        
        $this->content   = $content;
        $this->hasList   = $hasList;
        $this->listField = $listField;
        $this->blockName = $blockName;
    }
    
    /**
     * @return string
     */
    public function replace(): string
    {
        $this->fieldParser->setHasList($this->hasList)->setListField(false);
        $blockName      = $this->blockName;
        $configureTypes = null;
        $json           = $this->json;
        if ($this->hasList) {
            
            
            $this->content = str_replace('#HandlerExcludingFieldsList#', array_keys($this->json)[0], $this->content);
            
            $json = $this->json[$this->listField];
            $this->fieldParser->setListField($this->listField);
            
            $forList = self::getForList($json, $this->listField, $blockName);
            
            $configureTypes = $forList['configureTypes'];
            $json           = $forList['list'];
        }
     
        $result = $this->fieldParser->parse($json, $blockName, $configureTypes);
      
        $mainNameFieldName = $this->fieldParser->getMainNameFieldName($json, $blockName);
        
        $this->content = str_replace('#RepositoryMainNameField#', $mainNameFieldName, $this->content);
        
        foreach ($result as $keyTemplate => $item) {
            $this->content = str_replace('#' . ucfirst($keyTemplate) . '#', implode("\n", $item), $this->content);
        }
        
        return $this->content;
    }
    
    public static function getForList(array $json, string $listField, string $blockName): array
    {
        $fieldParser = new FieldParser();
        $fieldParser->setHasList(true)->setListField($listField);
        
        $configureTypes = [];
        
        $newJson = [];
        foreach ($json as $item) {
            $types = $fieldParser->getFieldTypes($item, $blockName);
            
            foreach ($types as $keyField => $type) {
                $typeFromConfig = $configureTypes[$keyField] ?? null;
                if (!$typeFromConfig) {
                    $configureTypes[$keyField] = $type;
                } else {
                    $configureTypes[$keyField] = $fieldParser::compareTypes($configureTypes[$keyField], $type);
                }
                
            }
            
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $newJson = array_merge($newJson, $item);
         
        }
    
        return ['list' => $newJson, 'configureTypes' => $configureTypes];
    }
}
