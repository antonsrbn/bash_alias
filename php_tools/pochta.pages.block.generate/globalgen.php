<?php

use Symfony\Component\VarDumper\VarDumper;
use Tools\Blocks\BlocksParser;
use Tools\Blocks\MigrationPropertyMaker;
use Tools\Blocks\PageMaker;
use Tools\CodeBlockLogicGenerator;
use Tools\Detectors\BlockDetector;
use Tools\Detectors\SeoMetaDetector;
use Tools\Detectors\TinaBlockDetector;
use Tools\FindFileCloner;
use Tools\Helper;
use Tools\NameTranslator;
use Tools\Storage;
use Tools\Tsx\Parser;

require __DIR__ . '/vendor/autoload.php';

$route   = $_SERVER['argv'][1];
$hasProd = $_SERVER['argv'][2] ?? false;

$projectPath  = '/home/anton/projects/pochtabank.ru';
$frontPath    = $projectPath . '/apps/front';
$frontSrcPath = $projectPath . '/apps/front/src';
$templatePath = '/home/anton/bash_alias/php_tools/pochta.pages.block.generate/templates';

if ($hasProd) {
    $absolutePath = $projectPath;
} else {
    $absolutePath = '/home/anton/bash_alias/php_tools/pochta.pages.block.generate/testing';
}

$migrationsPath = 'apps/back/db/migrations';
$sourcePath     = 'apps/back/db/source';

/** @noinspection PhpUnhandledExceptionInspection */
$dt         = new DateTime('now', new DateTimeZone('UTC'));
$datePrefix = (int)$dt->format('YmdHis');

$parser = new Parser($frontSrcPath, $route);

$data = $parser->getData();
if (!$data) {
    die("Не удалось прочитать моковский файл!((\n");
}

$seoMetaDetector = new SeoMetaDetector($data['meta'], $route);
$data['meta']    = $seoMetaDetector->detect()->getMeta();

$blocks = $data['blocks'];

$tinaBlockDetector = new TinaBlockDetector($frontSrcPath);
$tinaBlocks        = $tinaBlockDetector->detect(array_column($blocks, '_template'));

Storage::setTinaBlocks($tinaBlocks);

VarDumper::dump($tinaBlockDetector->getFailList());

$blockDetector           = new BlockDetector($projectPath, $absolutePath);
$codeBlockLogicGenerator = new CodeBlockLogicGenerator($templatePath, $absolutePath, $blockDetector);

$blockParser     = new BlocksParser($blocks, $templatePath, $route, $data['meta']['title'], $blockDetector);
$migrationData   = $blockParser->makeBlocksContent();
$blocksContent   = $migrationData->getAddBLocksContent();
$blocksForCreate = $migrationData->getBlocksForCreate();

$blockGenerateResult = $codeBlockLogicGenerator->generate($blocksForCreate);

VarDumper::dump($blockGenerateResult);
$migrationPropertyMaker = new MigrationPropertyMaker($migrationData->getFieldsForAppend(),
    $templatePath,
    $absolutePath,
    $migrationsPath,
    $datePrefix);

$clonerResult = (new FindFileCloner($blocks,
    $absolutePath . '/' . $sourcePath,
    $projectPath . '/' . $sourcePath,
    $frontPath))->copyFiles();

VarDumper::dump($clonerResult);

VarDumper::dump($migrationPropertyMaker->generate()->getLog());

$pageContent = (new PageMaker($data,
    $migrationData->getBlocksForStructure(),
    $templatePath,
    $route))->makePageContent();

$filePathBlocks = $absolutePath . '/' . $migrationsPath . '/' . ($datePrefix + Storage::tick()) . '_add_' . (new NameTranslator($route))->getFileCodeName() . '_blocks.php';
if (file_exists($filePathBlocks)) {
    echo "$filePathBlocks уже существует\n";
} else {
    Helper::makeDirectories($absolutePath . '/' . $migrationsPath);
    
    $result = file_put_contents($filePathBlocks, $blocksContent);
    if ($result) {
        echo "$filePathBlocks создан\n";
    }
}

$filePathPage = $absolutePath . '/' . $migrationsPath . '/' . ($datePrefix + Storage::tick()) . '_add_' . (new NameTranslator($route))->getFileCodeName() . '_page.php';
if (file_exists($filePathPage)) {
    echo "$filePathPage уже существует";
} else {
    Helper::makeDirectories($absolutePath . '/' . $migrationsPath);
    
    $result = file_put_contents($filePathPage, $pageContent);
    if ($result) {
        echo "$filePathPage создан\n";
    }
}

