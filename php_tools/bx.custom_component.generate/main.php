<?php
$componentName = $_SERVER['argv'][1];
$concreteLocalPath = $_SERVER['argv'][2];


function callbackFunction( $matches) {
	return mb_strtoupper($matches[2]);
 }

$pattern = '/([^A-Za-z]{1})([A-Za-z]{1})/ui';


$className = mb_strtolower($componentName);
$className = trim(preg_replace('/\s/ui', '', $className));

$className = preg_replace_callback($pattern, 'callbackFunction', $className, -1);

$className = preg_replace('/[^\dA-Za-z]/ui', '', $className);
$className = preg_replace('/^\d/ui', '', $className);
$className = ucfirst($className);


copy_directory(__DIR__ . '/custom_component', __DIR__ . '/' . $componentName);
$classFilePath =  __DIR__ . '/' . $componentName . '/class.php';

$classFileContent = file_get_contents($classFilePath);
$classFileContent = str_replace('#CLASS_NAME#', $className, $classFileContent);
file_put_contents($classFilePath, $classFileContent);


copy_directory(__DIR__ . '/' . $componentName, $concreteLocalPath . $componentName);

rrmdir(__DIR__ . '/' . $componentName);





function copy_directory($src, $dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                copy_directory($src . '/' . $file,$dst . '/' . $file);
            }
            else {
                copy($src . '/' . $file,$dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir);
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
           rrmdir($dir. DIRECTORY_SEPARATOR .$object);
         else
           unlink($dir. DIRECTORY_SEPARATOR .$object); 
       } 
     }
     rmdir($dir); 
   } 
 }