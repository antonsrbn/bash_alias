<?php
/** @noinspection AutoloadingIssuesInspection */

/** @noinspection PhpUndefinedClassInspection */

namespace Components\Custom;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use CBitrixComponent;
use Exception;
use RuntimeException;

/**
 * Class #CLASS_NAME#.
 * #Здесь должно быть описание компонента#.
 *
 * @package Components\Custom
 */
class #CLASS_NAME# extends CBitrixComponent
{
    /**
     * #CLASS_NAME# constructor.
     *
     * @param null $component
     * @throws LoaderException
     * @throws Exception
     */
    public function __construct($component = null)
    {
        $this->loadModules();
        $this->arResult = [];

        parent::__construct($component);
    }

    /**
     * Вызвать компонент.
     *
     * @return void
     */
    public function executeComponent(): void
    {
        /**
         * @TODO Написать код компонента.
         */
        $this->includeComponentTemplate();
    }

    /**
     * Загрузить модули.
     *
     * @param string ...$modules
     * @throws LoaderException
     * @throws Exception
     */
    private function loadModules(...$modules): void
    {
        foreach ($modules as $module) {
            if (!Loader::includeModule($module)) {
                throw new RuntimeException("Не удалось загрузить модуль `{$module}`.");
            }
        }
    }
}
