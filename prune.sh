#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

docker container prune;
docker image prune;
docker network prune;
echo -e "\e[92mКонтейнеры почищены";
