#!/bin/bash

source $(dirname $0)/includes/helpers.sh

checkLinkDirs;
cd "$executingDir";

docker container prune -f;