#!/bin/bash

cd  /home/$USER/projects/web-dev;
docker-compose exec -uphp php bash -c 'if [[ $(grep "alias test=" /home/php/.bashrc) = "" ]]; then echo "alias test=\"./../../../vendor/bin/codecept run\"" >> /home/php/.bashrc; fi';
docker-compose exec -uphp php bash -c 'if [[ $(grep "alias mg=" /home/php/.bashrc) = "" ]]; then echo "alias mg=\"./../../../vendor/bin/robo parallel:migrate-up\"" >> /home/php/.bashrc; fi';
docker-compose exec -uphp --workdir=/var/www/html/tests/codeception/frontend php bash;
